package sample;

import database.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class JobOfferEditing extends Application{
    private static JobOfferEditing instance;
    private static Company company;
    private ListView<HBox> languagesWithLevelsList;
    private ArrayList<ChoiceBox<String >> levelsChoiceBoxList;
    private ArrayList<Language> languagesList;
    TextField nameOfOffer;
    DatePicker dateOfExpire;
    TextField minExpirience;
    ChoiceBox<String> partOfVacancy;
    ChoiceBox<String> typeOfEducation;
    ChoiceBox<String> typeOfContract;
    ChoiceBox<String> typeOfJob;
    ChoiceBox<String> field;
    TextField city;
    TextField minSalary;
    Label noDate;
    Label noName;
    Label onlyNumbersForExpirience;
    Label onlyNumbersForSalary;
    ArrayList<RequiredLanguage> requiredLanguagesForJobOffer;


    public static JobOfferEditing getInstance() {
        return instance;
    }

    public JobOfferEditing() {
        instance = this;
    }

    @FXML
    private ScrollPane jobOffersPane;

    @FXML
    private ListView<HBox> jobOffersListView;

    @FXML
    private AnchorPane jobOffersList;

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/java/sample/jobOfferEditing.fxml").toURL(); // todo: change that path
        Parent root = new AnchorPane();
        try{
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        primaryStage.setTitle("Editing");
        primaryStage.setScene(new Scene(root));
        //primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public void createPage(Pane jobOfferCreating) throws MalformedURLException {
        URL url = new File( "src/main/java/sample/jobOfferCreating.fxml").toURL();
        try {
            jobOfferCreating = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        loadJobOffers();
    }

    private void loadJobOffers() {
        DAOManager dm = DAOManager.getInstance();
        try {
            CompanyDAO companyDAO = dm.getCompanyDAO();
            int id = SignInLogic.loggedUser.getIdUser();
            int companyID = companyDAO.getCompanyIDByUserID(id);
            company = companyDAO.getCompanyByID(companyID);

            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            ArrayList<JobOffer> jobOffers = jobOfferDAO.getJobOfferListByCompanyId(company.getIdCompany());

            jobOffersListView.getItems().clear();
            for(JobOffer jobOffer : jobOffers) {
                HBox hBox = new HBox();
                Label jobOfferName = new Label(jobOffer.getnameOfOffer());
                jobOfferName.setPrefSize(500, 10);
                Button deleteBtn = new Button("DELETE");

                jobOfferName.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if(e.getClickCount() == 2){
                            showOfferDetails(jobOffer);
                        }
                    }
                });

                deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                        try {
                            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
                            requiredLanguageDAO.deleteRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer());
                            AplicationDAO aplicationDAO = dm.getAplicationDAO();
                            aplicationDAO.deleteAplicationByJobOfferId(jobOffer.getIdJobOffer());
                            jobOfferDAO.deleteJobOfferById(jobOffer.getIdJobOffer());
                            loadJobOffers();
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

                hBox.getChildren().add(jobOfferName);
                hBox.getChildren().add(deleteBtn);
                hBox.setSpacing(100.0);

                jobOffersListView.getItems().add(hBox);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showOfferDetails(JobOffer jobOffer) {
        GridPane gridPane = new GridPane();
        VBox vbox = new VBox();

        DAOManager dm = DAOManager.getInstance();

        try {
            Label offeraName = new Label(jobOffer.getnameOfOffer());
            Label companyName = new Label("Company: " + company.getName());
            vbox.getChildren().add(companyName);

            int numberOfRowSpan = 0;

            if (jobOffer.getMinExpirience() != 0) {
                Label minExpirience = new Label("Min Expirience: " + jobOffer.getMinExpirience());
                vbox.getChildren().add(minExpirience);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdPartOfVacancy() != 0) {
                PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
                PartOfVacancy partOfVacancy = partOfVacancyDAO.getPartOfVacancyById(jobOffer.getIdPartOfVacancy());
                Label partOfVacancyLabel = new Label("Part of vacancy: " + partOfVacancy.getName());
                vbox.getChildren().add(partOfVacancyLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfEducation() != 0) {
                TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
                TypeOfEducation typeOfEducation = typeOfEducationDAO.getTypeOfEducationById(jobOffer.getIdTypeOfEducation());
                Label typeOfEducationLabel = new Label("Type of Education: " + typeOfEducation.getName());
                vbox.getChildren().add(typeOfEducationLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfConcract() != 0) {
                TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
                TypeOfContract typeOfContract = typeOfContractDAO.getTypeOfContractById(jobOffer.getIdTypeOfConcract());
                Label typeOfContractLabel = new Label("Type of contract: " + typeOfContract.getName());
                vbox.getChildren().add(typeOfContractLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfJob() != 0) {
                TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
                TypeOfJob typeOfJob = typeOfJobDAO.getTypeOfJobById(jobOffer.getIdTypeOfJob());
                Label typeOfJobLabel = new Label("Type of job: " + typeOfJob.getName());
                vbox.getChildren().add(typeOfJobLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getCity() != null) {
                Label city = new Label("City: " + jobOffer.getCity());
                vbox.getChildren().add(city);
                ++numberOfRowSpan;
            }

            if (jobOffer.getMinSalary() != 0) {
                Label salary = new Label("Salary: " + jobOffer.getMinSalary());
                vbox.getChildren().add(salary);
                ++numberOfRowSpan;
            }

            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            if (!requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer()).isEmpty()) {
                HBox requiredLanguages = new HBox();
                VBox requiredLanguagesList = new VBox();
                Label requiredLanguagesLabel = new Label("Required languages: ");
                for (RequiredLanguage requiredLanguage : requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer())) {
                    Label requiredLanguageLabel = new Label(requiredLanguage.toString());
                    requiredLanguagesList.getChildren().add(requiredLanguageLabel);
                    ++numberOfRowSpan;
                }

                vbox.getChildren().add(requiredLanguages);
                requiredLanguages.getChildren().addAll(requiredLanguagesLabel, requiredLanguagesList);
            }

            Button back = new Button("Back");
            Button edit = new Button("Edit");

            back.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    loadJobOffers();
                    jobOffersPane.setContent(jobOffersList);
                }
            });

            edit.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    showJobOfferEditWindow(jobOffer);
                }
            });

            gridPane.add(offeraName, 2, 1);
            gridPane.add(vbox, 3, 3, 2, numberOfRowSpan);
            gridPane.add(edit,5, 30);
            gridPane.add(back,1,30);

            jobOffersPane.setContent(gridPane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showJobOfferEditWindow(JobOffer jobOffer) {
        AnchorPane jobOfferForm = new AnchorPane();
        languagesWithLevelsList = new ListView<>();
        partOfVacancy = new ChoiceBox<>();
        typeOfEducation = new ChoiceBox<>();
        typeOfContract = new ChoiceBox<>();
        typeOfJob = new ChoiceBox<>();
        field = new ChoiceBox<>();
        nameOfOffer = new TextField();
        dateOfExpire = new DatePicker();
        minExpirience = new TextField();
        minSalary = new TextField();
        city = new TextField();
        noDate = new Label("No date!!!");
        noDate.setTextFill(Color.RED);
        noDate.setVisible(false);
        noName = new Label("No name!!!");
        noName.setTextFill(Color.RED);
        noName.setVisible(false);
        onlyNumbersForExpirience = new Label("Only numbers!!!");
        onlyNumbersForExpirience.setTextFill(Color.RED);
        onlyNumbersForExpirience.setVisible(false);
        onlyNumbersForSalary = new Label("Only numbers!!!");
        onlyNumbersForSalary.setTextFill(Color.RED);
        onlyNumbersForSalary.setVisible(false);
        Label editLabel = new Label("Edit Offer");
        editLabel.setFont(Font.font(24.0));
        Label partOfvacancyLabel = new Label("Part of vacancy");
        Label reqTypeOfEducationLabel = new Label("Req type of education");
        Label typeOfContractLabel = new Label("Type of contract");
        Label typeOfJobLabel = new Label("Type of job");
        Label fieldLabel = new Label("Field");
        requiredLanguagesForJobOffer = new ArrayList<>();

        DAOManager dm = DAOManager.getInstance();
        try {

            nameOfOffer.setText(jobOffer.getnameOfOffer());
            dateOfExpire.setValue(jobOffer.getDateOfExpire().toLocalDate());
            minExpirience.setPromptText("min expirience [years]");
            if(jobOffer.getMinExpirience() != 0) {
                minExpirience.setText(String.valueOf(jobOffer.getMinExpirience()));
            }

            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            partOfVacancy.getItems().add(new String("None"));
            if(jobOffer.getIdPartOfVacancy() != 0) {
                partOfVacancy.setValue(new String(partOfVacancyDAO.getPartOfVacancyById(jobOffer.getIdPartOfVacancy()).getName()));

            } else {
                partOfVacancy.setValue(new String("None"));
            }

            for(PartOfVacancy vacancy : partOfVacancyDAO.getAllPartsOfVacancies()) {
                partOfVacancy.getItems().add(vacancy.getName());
            }

            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            typeOfEducation.setValue(new String(typeOfEducationDAO.getTypeOfEducationById(jobOffer.getIdTypeOfEducation()).getName()));

            for(TypeOfEducation education : typeOfEducationDAO.getAllTypesOfEducations()) {
                typeOfEducation.getItems().add(education.getName());
            }

            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            typeOfContract.getItems().add(new String("None"));
            if(jobOffer.getIdTypeOfConcract() != 0) {
                typeOfContract.setValue(new String(typeOfContractDAO.getTypeOfContractById(jobOffer.getIdTypeOfConcract()).getName()));
            } else {
                typeOfContract.setValue(new String("None"));
            }

            for(TypeOfContract contract : typeOfContractDAO.getAllTypesOfContracts()) {
                typeOfContract.getItems().add(contract.getName());
            }

            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            typeOfJob.getItems().add(new String("None"));
            if(jobOffer.getIdTypeOfJob() != 0) {
                typeOfJob.setValue(new String(typeOfJobDAO.getTypeOfJobById(jobOffer.getIdTypeOfJob()).getName()));
            } else {
                typeOfJob.setValue(new String("None"));
            }

            for(TypeOfJob job : typeOfJobDAO.getAllTypesOfJobs()) {
                typeOfJob.getItems().add(job.getName());
            }

            FieldDAO fieldDAO = dm.getFieldDAO();
            field.getItems().add(new String("None"));
            if(jobOffer.getIdField() != 0) {
                field.setValue(new String(fieldDAO.getFieldById(jobOffer.getIdField()).getName()));
            } else {
                field.setValue(new String("None"));
            }

            for(Field fieldOfOffer : fieldDAO.getAllFields()) {
                field.getItems().add(fieldOfOffer.getName());
            }

            languagesWithLevelsList = new ListView<>();
            levelsChoiceBoxList = new ArrayList<>();
            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            LanguageDAO languageDAO = dm.getLanguageDAO();
            languagesList = languageDAO.getAllLanguages();
            LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            ArrayList<LanguageLevel> languageLevels = languageLevelDAO.getAllLanguageLevels();

            for(int i = 0; i < languagesList.size(); ++i) {
                HBox languageWithLevels = new HBox();
                Label languageName = new Label(languagesList.get(i).getName() + ": ");
                languageWithLevels.getChildren().add(languageName);
                ChoiceBox<String> levelsForLanguage = new ChoiceBox<>();

                for(int j = 0; j < languageLevels.size(); ++j) {
                    String levelName = new String(languageLevels.get(j).getName());
                    levelsForLanguage.getItems().add(levelName);
                }
                levelsForLanguage.getItems().add(new String("Not Required"));
                ArrayList<Integer> language = new ArrayList<>();
                language.add(languagesList.get(i).getIdLanguage());
                requiredLanguagesForJobOffer = requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer());

                if(requiredLanguageDAO.isJobOfferRequiresAnyOfLanguages(jobOffer.getIdJobOffer(), language)) {
                    RequiredLanguage requiredLanguage = requiredLanguageDAO.getRquierdLanguagesByJobOfferIdAndLanguageId(jobOffer.getIdJobOffer(), languagesList.get(i).getIdLanguage());
                    levelsForLanguage.setValue(new String(languageLevelDAO.getLanguageLevelById(requiredLanguage.getIdLanguageLevel()).getName()));
                } else {
                    levelsForLanguage.setValue(new String("Not Required"));
                }
                levelsChoiceBoxList.add(levelsForLanguage);

                languageWithLevels.getChildren().add(levelsForLanguage);
                languagesWithLevelsList.getItems().add(languageWithLevels);
            }

            city.setPromptText("City");
            if(jobOffer.getCity() != null) {
                city.setText(jobOffer.getCity());
            }

            minSalary.setPromptText("Salary");
            if(jobOffer.getMinSalary() != 0) {
                minSalary.setText(String.valueOf(jobOffer.getMinSalary()));
            }

            Button updateBtn = new Button("Update");
            Button backBtn = new Button("Back");

            updateBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    updateJobOffer(jobOffer);
                }
            });

            backBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    showOfferDetails(jobOffer);
                }
            });

            languagesWithLevelsList.setMaxSize(1400, 300);

            jobOfferForm.getChildren().add(nameOfOffer);
            jobOfferForm.setTopAnchor(nameOfOffer, 67.0);
            jobOfferForm.setLeftAnchor(nameOfOffer, 63.0);
            jobOfferForm.setRightAnchor(nameOfOffer, 500.0);

            jobOfferForm.getChildren().add(dateOfExpire);
            jobOfferForm.setTopAnchor(dateOfExpire, 114.0);
            jobOfferForm.setLeftAnchor(dateOfExpire, 63.0);
            jobOfferForm.setRightAnchor(dateOfExpire, 469.0);

            jobOfferForm.getChildren().add(minExpirience);
            jobOfferForm.setTopAnchor(minExpirience, 161.0);
            jobOfferForm.setLeftAnchor(minExpirience, 63.0);
            jobOfferForm.setRightAnchor(minExpirience, 500.0);

            jobOfferForm.getChildren().add(partOfVacancy);
            jobOfferForm.setTopAnchor(partOfVacancy,210.0);
            jobOfferForm.setLeftAnchor(partOfVacancy, 186.0);
            jobOfferForm.setRightAnchor(partOfVacancy, 414.0);

            jobOfferForm.getChildren().add(partOfvacancyLabel);
            jobOfferForm.setTopAnchor(partOfvacancyLabel,215.0);
            jobOfferForm.setLeftAnchor(partOfvacancyLabel, 63.0);
            jobOfferForm.setRightAnchor(partOfvacancyLabel, 586.0);

            jobOfferForm.getChildren().add(reqTypeOfEducationLabel);
            jobOfferForm.setTopAnchor(reqTypeOfEducationLabel, 255.0);
            jobOfferForm.setLeftAnchor(reqTypeOfEducationLabel, 63.0);
            jobOfferForm.setRightAnchor(reqTypeOfEducationLabel, 536.0);

            jobOfferForm.getChildren().add(typeOfEducation);
            jobOfferForm.setTopAnchor(typeOfEducation, 250.0);
            jobOfferForm.setLeftAnchor(typeOfEducation, 233.0);
            jobOfferForm.setRightAnchor(typeOfEducation, 367.0);

            jobOfferForm.getChildren().add(typeOfContractLabel);
            jobOfferForm.setTopAnchor(typeOfContractLabel, 295.0);
            jobOfferForm.setLeftAnchor(typeOfContractLabel, 63.0);
            jobOfferForm.setRightAnchor(typeOfContractLabel, 579.0);

            jobOfferForm.getChildren().add(typeOfContract);
            jobOfferForm.setTopAnchor(typeOfContract, 290.0);
            jobOfferForm.setLeftAnchor(typeOfContract, 193.0);
            jobOfferForm.setRightAnchor(typeOfContract, 407.0);

            jobOfferForm.getChildren().add(typeOfJobLabel);
            jobOfferForm.setTopAnchor(typeOfJobLabel, 335.0);
            jobOfferForm.setLeftAnchor(typeOfJobLabel,63.0 );
            jobOfferForm.setRightAnchor(typeOfJobLabel, 610.0);

            jobOfferForm.getChildren().add(typeOfJob);
            jobOfferForm.setTopAnchor(typeOfJob, 330.0);
            jobOfferForm.setLeftAnchor(typeOfJob, 158.0);
            jobOfferForm.setRightAnchor(typeOfJob, 442.0);

            jobOfferForm.getChildren().add(fieldLabel);
            jobOfferForm.setTopAnchor(fieldLabel, 375.0);
            jobOfferForm.setLeftAnchor(fieldLabel, 63.0);
            jobOfferForm.setRightAnchor(fieldLabel, 655.0);

            jobOfferForm.getChildren().add(city);
            jobOfferForm.setTopAnchor(city, 421.0);
            jobOfferForm.setLeftAnchor(city, 63.0);
            jobOfferForm.setRightAnchor(city, 500.0);

            jobOfferForm.getChildren().add(minSalary);
            jobOfferForm.setTopAnchor(minSalary, 468.0);
            jobOfferForm.setLeftAnchor(minSalary, 63.0);
            jobOfferForm.setRightAnchor(minSalary, 500.0);

            jobOfferForm.getChildren().add(updateBtn);
            jobOfferForm.setBottomAnchor(updateBtn, 30.0);
            jobOfferForm.setLeftAnchor(updateBtn, 561.0);

            jobOfferForm.getChildren().add(backBtn);
            jobOfferForm.setBottomAnchor(backBtn, 80.0);
            jobOfferForm.setLeftAnchor(backBtn, 561.0);

            jobOfferForm.getChildren().add(field);
            jobOfferForm.setTopAnchor(field, 370.0);
            jobOfferForm.setLeftAnchor(field, 111.0);
            jobOfferForm.setRightAnchor(field, 489.0);

            jobOfferForm.getChildren().add(noName);
            jobOfferForm.setTopAnchor(noName, 72.0);
            jobOfferForm.setLeftAnchor(noName, 383.0);

            jobOfferForm.getChildren().add(noDate);
            jobOfferForm.setTopAnchor(noDate, 119.0);
            jobOfferForm.setLeftAnchor(noDate, 391.0);

            jobOfferForm.getChildren().add(onlyNumbersForExpirience);
            jobOfferForm.setTopAnchor(onlyNumbersForExpirience, 166.0);
            jobOfferForm.setLeftAnchor(onlyNumbersForExpirience, 364.0);

            jobOfferForm.getChildren().add(onlyNumbersForSalary);
            jobOfferForm.setTopAnchor(onlyNumbersForSalary, 473.0);
            jobOfferForm.setLeftAnchor(onlyNumbersForSalary, 374.0);

            jobOfferForm.getChildren().add(languagesWithLevelsList);
            jobOfferForm.setTopAnchor(languagesWithLevelsList, 515.0);
            jobOfferForm.setLeftAnchor(languagesWithLevelsList, 63.0);

            jobOfferForm.getChildren().add(editLabel);
            jobOfferForm.setTopAnchor(editLabel, 21.0);
            jobOfferForm.setLeftAnchor(editLabel, 300.0);
            jobOffersPane.setContent(jobOfferForm);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateJobOffer(JobOffer jobOffer) {
        DAOManager dm = DAOManager.getInstance();

        String name = new String();
        java.sql.Date date = new java.sql.Date(2020, 1, 1);
        float expirience = 0;
        String cityForJobOffer = new String();
        float salary = 0;

        boolean isEverythingOk = true;
        if(nameOfOffer.getText().isEmpty()) {
            noName.setVisible(true);
            isEverythingOk = false;
        } else {
            noName.setVisible(false);
            name = nameOfOffer.getText();
        }

        if(dateOfExpire.getValue() == null) {
            noDate.setVisible(true);
            isEverythingOk = false;
        } else {
            noDate.setVisible(false);
            date = java.sql.Date.valueOf(dateOfExpire.getValue());
        }

        try{
            onlyNumbersForExpirience.setVisible(false);
            if((!minExpirience.getText().toString().isEmpty())){
                expirience = Float.parseFloat(minExpirience.getText());

            }
        } catch (NumberFormatException e) {
            onlyNumbersForExpirience.setVisible(true);
            isEverythingOk = false;
        }

        cityForJobOffer = city.getText().toString();

        try{
            onlyNumbersForSalary.setVisible(false);
            if((!minSalary.getText().toString().isEmpty())) {
                salary = Float.parseFloat(minSalary.getText());
            }
        } catch (NumberFormatException e) {
            onlyNumbersForSalary.setVisible(true);
            isEverythingOk = false;
        }

        if(!isEverythingOk) {
            return;
        }

        try {
            company = dm.getCompanyDAO().getCompanyByID(1);
            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            jobOffer.setDateOfExpire(date);
            jobOffer.setnameOfOffer(name);
            jobOffer.setCity(cityForJobOffer);
            jobOffer.setMinSalary(salary);
            jobOffer.setMinExpirience(expirience);

            String namePartOfVacancy = partOfVacancy.getValue();
            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            PartOfVacancy selectedPartOfVacancy = partOfVacancyDAO.getPartOfVacancyByName(namePartOfVacancy);
            Integer idPartOfVacancy = null;
            if(selectedPartOfVacancy != null) {
                idPartOfVacancy = selectedPartOfVacancy.getIdPartOfVacancy();
                jobOffer.setIdPartOfVacancy(idPartOfVacancy);
            }

            String nameTypeOfEducation = typeOfEducation.getValue();
            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            TypeOfEducation selectedTypeOfEducation = typeOfEducationDAO.getTypeOfEducationByName(nameTypeOfEducation);
            Integer idTypeOfEducation = null;
            if(selectedTypeOfEducation != null) {
                idTypeOfEducation = selectedTypeOfEducation.getIdTypeOfEducation();
                jobOffer.setIdTypeOfEducation(idTypeOfEducation);
            }

            String nameOfTypeOfContract = typeOfContract.getValue();
            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            TypeOfContract selectedTypeOfContract = typeOfContractDAO.getTypeOfContractByName(nameOfTypeOfContract);
            Integer idTypeOfContract = null;
            if(selectedTypeOfContract != null) {
                idTypeOfContract = selectedTypeOfContract.getIdTypeOfContract();
                jobOffer.setIdTypeOfConcract(idTypeOfContract);
            }

            String nameOfTypeOfJob = typeOfJob.getValue();
            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            TypeOfJob selectedTypeOfJob = typeOfJobDAO.getTypeOfJobByName(nameOfTypeOfJob);
            Integer idTypeOfJob = null;
            if(selectedTypeOfJob != null) {
                idTypeOfJob = selectedTypeOfJob.getIdTypeOfJob();
                jobOffer.setIdTypeOfJob(idTypeOfJob);
            }

            String nameOfField = field.getValue();
            FieldDAO fieldDAO = dm.getFieldDAO();
            Field selectedField = fieldDAO.getFieldByName(nameOfField);
            Integer idField = null;
            if(selectedField != null) {
                idField = selectedField.getIdField();
                jobOffer.setIdField(idField);
            }
            jobOfferDAO.updateJobOffer(jobOffer);

            ArrayList<RequiredLanguage> requiredLanguages = new ArrayList<>();
            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            for(int i = 0; i < languagesList.size(); ++i) {
                ChoiceBox<String> levelsForLanguage = levelsChoiceBoxList.get(i);
                Language selectedLanguage = languagesList.get(i);
                LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
                if(!levelsForLanguage.getValue().toString().equals(new String("Not Required"))) {
                    LanguageLevel languageLevel = languageLevelDAO.getLanguageLevelByName(levelsForLanguage.getValue().toString());
                    RequiredLanguage requiredLanguage = new RequiredLanguage(jobOffer.getIdJobOffer(), languageLevel.getIdLanguageLevel(), selectedLanguage.getIdLanguage());
                    requiredLanguages.add(requiredLanguage);
                    if(!requiredLanguagesForJobOffer.contains(requiredLanguage)) {
                        requiredLanguageDAO.create(requiredLanguage);
                    }
                }
            }

            for(RequiredLanguage requiredLanguage : requiredLanguagesForJobOffer) {
                if(!requiredLanguages.contains(requiredLanguage)) {
                    requiredLanguageDAO.deleteRequiredLanguageById(requiredLanguage.getIdRequiredLanguage());
                }
            }

            showOfferDetails(jobOffer);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Company getCompany() {
        return company;
    }

    public static void setCompany(Company company) {
        JobOfferEditing.company = company;
    }

    public static void main(String[] args) {
        launch(args);
    }
}

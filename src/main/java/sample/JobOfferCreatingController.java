package sample;

import database.*;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

public class JobOfferCreatingController extends Application {
    private static JobOfferCreatingController instance;
    private static Company company;
    private static ArrayList<Language> languages;
    private ListView<HBox> languagesWithLevelsList;
    private ArrayList<ChoiceBox<String >> levelsChoiceBoxList;
    private ArrayList<Language> languagesList;

    public static JobOfferCreatingController getInstance() {
        return instance;
    }

    public JobOfferCreatingController() {
        instance = this;
    }

    @FXML
    AnchorPane JobOfferForm;

    @FXML
    TextField nameOfOffer;

    @FXML
    DatePicker dateOfExpire;

    @FXML
    TextField minExpirience;

    @FXML
    ChoiceBox<String> partOfVacancy;

    @FXML
    ChoiceBox<String> typeOfEducation;

    @FXML
    ChoiceBox<String> typeOfContract;

    @FXML
    ChoiceBox<String> typeOfJob;

    @FXML
    ChoiceBox<String> field;

    @FXML
    TextField city;

    @FXML
    TextField minSalary;

    @FXML
    Button creataBtn;

    @FXML
    Button eraseBtn;

    @FXML
    Label noDate;

    @FXML
    Label noName;

    @FXML
    Label onlyNumbersForExpirience;

    @FXML
    Label onlyNumbersForSalary;

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/java/sample/jobOfferCreating.fxml").toURL(); // todo: change that path
        Parent root = new AnchorPane();
        try{
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        primaryStage.setTitle("Creating");
        primaryStage.setScene(new Scene(root));
        //primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public void createPage(Pane jobOfferCreating) throws MalformedURLException {
        URL url = new File( "src/main/java/sample/jobOfferCreating.fxml").toURL();
        try {
            jobOfferCreating = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        languages = new ArrayList<>();
        createNewJobOfferWindow();
    }

    private void createNewJobOfferWindow(){
        DAOManager dm = DAOManager.getInstance();
        try {
            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            partOfVacancy.getItems().add(new String("None"));
            partOfVacancy.setValue(new String("None"));

            for(PartOfVacancy vacancy : partOfVacancyDAO.getAllPartsOfVacancies()) {
                partOfVacancy.getItems().add(vacancy.getName());
            }

            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            typeOfEducation.setValue(new String("None"));

            for(TypeOfEducation education : typeOfEducationDAO.getAllTypesOfEducations()) {
                typeOfEducation.getItems().add(education.getName());
            }

            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            typeOfContract.getItems().add(new String("None"));
            typeOfContract.setValue(new String("None"));

            for(TypeOfContract contract : typeOfContractDAO.getAllTypesOfContracts()) {
                typeOfContract.getItems().add(contract.getName());
            }

            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            typeOfJob.getItems().add(new String("None"));
            typeOfJob.setValue(new String("None"));

            for(TypeOfJob job : typeOfJobDAO.getAllTypesOfJobs()) {
                typeOfJob.getItems().add(job.getName());
            }

            FieldDAO fieldDAO = dm.getFieldDAO();
            field.getItems().add(new String("None"));
            field.setValue(new String("None"));

            for(Field fieldOfOffer : fieldDAO.getAllFields()) {
                field.getItems().add(fieldOfOffer.getName());
            }

            languagesWithLevelsList = new ListView<>();
            levelsChoiceBoxList = new ArrayList<>();
            LanguageDAO languageDAO = dm.getLanguageDAO();
            languagesList = languageDAO.getAllLanguages();
            LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            ArrayList<LanguageLevel> languageLevels = languageLevelDAO.getAllLanguageLevels();

            for(int i = 0; i < languagesList.size(); ++i) {
                HBox languageWithLevels = new HBox();
                Label languageName = new Label(languagesList.get(i).getName() + ": ");
                languageWithLevels.getChildren().add(languageName);
                ChoiceBox<String> levelsForLanguage = new ChoiceBox<>();

                for(int j = 0; j < languageLevels.size(); ++j) {
                    String levelName = new String(languageLevels.get(j).getName());
                    levelsForLanguage.getItems().add(levelName);
                }
                levelsForLanguage.getItems().add(new String("Not Required"));
                levelsForLanguage.setValue(new String("Not Required"));
                levelsChoiceBoxList.add(levelsForLanguage);

                languageWithLevels.getChildren().add(levelsForLanguage);
                languagesWithLevelsList.getItems().add(languageWithLevels);
            }

            languagesWithLevelsList.setMaxSize(1400, 300);
            JobOfferForm.getChildren().add(languagesWithLevelsList);
            JobOfferForm.setTopAnchor(languagesWithLevelsList, (double) 515);
            JobOfferForm.setLeftAnchor(languagesWithLevelsList, (double) 63);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void createBtnAction(){
        createJobOffer();
        erase();
        JobOfferEditing jOE = JobOfferEditing.getInstance();
        jOE.initialize();
    }

    private void createJobOffer() {
        DAOManager dm = DAOManager.getInstance();

        String name = new String();
        Date date = new Date(2020, 1, 1);
        float expirience = 0;
        String cityForJobOffer = new String();
        float salary = 0;

        boolean isEverythingOk = true;
        if(nameOfOffer.getText().isEmpty()) {
            noName.setVisible(true);
            isEverythingOk = false;
        } else {
            noName.setVisible(false);
            name = nameOfOffer.getText();
        }

        if(dateOfExpire.getValue() == null) {
            noDate.setVisible(true);
            isEverythingOk = false;
        } else {
            noDate.setVisible(false);
            date = Date.valueOf(dateOfExpire.getValue());
        }

        try{
            onlyNumbersForExpirience.setVisible(false);
            if((!minExpirience.getText().toString().isEmpty())){
                expirience = Float.parseFloat(minExpirience.getText());

            }
        } catch (NumberFormatException e) {
            onlyNumbersForExpirience.setVisible(true);
            isEverythingOk = false;
        }

        cityForJobOffer = city.getText().toString();

        try{
            onlyNumbersForSalary.setVisible(false);
            if((!minSalary.getText().toString().isEmpty())) {
                salary = Float.parseFloat(minSalary.getText());
            }
        } catch (NumberFormatException e) {
            onlyNumbersForSalary.setVisible(true);
            isEverythingOk = false;
        }

        if(!isEverythingOk) {
            return;
        }

        try {
            CompanyDAO companyDAO = dm.getCompanyDAO();
            int id = SignInLogic.loggedUser.getIdUser();
            int companyID = companyDAO.getCompanyIDByUserID(id);
            company = dm.getCompanyDAO().getCompanyByID(companyID);
            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            JobOffer jobOffer = new JobOffer(name, date, company.getIdCompany());
            jobOffer.setCity(cityForJobOffer);
            jobOffer.setMinSalary(salary);
            jobOffer.setMinExpirience(expirience);

            String namePartOfVacancy = partOfVacancy.getValue();
            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            PartOfVacancy selectedPartOfVacancy = partOfVacancyDAO.getPartOfVacancyByName(namePartOfVacancy);
            Integer idPartOfVacancy = null;
            if(selectedPartOfVacancy != null) {
                idPartOfVacancy = selectedPartOfVacancy.getIdPartOfVacancy();
                jobOffer.setIdPartOfVacancy(idPartOfVacancy);
            }

            String nameTypeOfEducation = typeOfEducation.getValue();
            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            TypeOfEducation selectedTypeOfEducation = typeOfEducationDAO.getTypeOfEducationByName(nameTypeOfEducation);
            Integer idTypeOfEducation = null;
            if(selectedTypeOfEducation != null) {
                idTypeOfEducation = selectedTypeOfEducation.getIdTypeOfEducation();
                jobOffer.setIdTypeOfEducation(idTypeOfEducation);
            }

            String nameOfTypeOfContract = typeOfContract.getValue();
            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            TypeOfContract selectedTypeOfContract = typeOfContractDAO.getTypeOfContractByName(nameOfTypeOfContract);
            Integer idTypeOfContract = null;
            if(selectedTypeOfContract != null) {
                idTypeOfContract = selectedTypeOfContract.getIdTypeOfContract();
                jobOffer.setIdTypeOfConcract(idTypeOfContract);
            }

            String nameOfTypeOfJob = typeOfJob.getValue();
            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            TypeOfJob selectedTypeOfJob = typeOfJobDAO.getTypeOfJobByName(nameOfTypeOfJob);
            Integer idTypeOfJob = null;
            if(selectedTypeOfJob != null) {
                idTypeOfJob = selectedTypeOfJob.getIdTypeOfJob();
                jobOffer.setIdTypeOfJob(idTypeOfJob);
            }

            String nameOfField = field.getValue();
            FieldDAO fieldDAO = dm.getFieldDAO();
            Field selectedField = fieldDAO.getFieldByName(nameOfField);
            Integer idField = null;
            if(selectedField != null) {
                idField = selectedField.getIdField();
                jobOffer.setIdField(idField);
            }

            jobOfferDAO.create(jobOffer);

            for(int i = 0; i < languagesList.size(); ++i) {
                ChoiceBox<String> levelsForLanguage = levelsChoiceBoxList.get(i);
                Language selectedLanguage = languagesList.get(i);
                RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
                LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
                if(!levelsForLanguage.getValue().toString().equals(new String("Not Required"))) {
                    LanguageLevel languageLevel = languageLevelDAO.getLanguageLevelByName(levelsForLanguage.getValue().toString());
                    requiredLanguageDAO.create(new RequiredLanguage(jobOffer.getIdJobOffer(), languageLevel.getIdLanguageLevel(), selectedLanguage.getIdLanguage()));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void eraseBtnAction(){
        erase();
    }

    private void erase() {
        languages = new ArrayList<>();

        if(!nameOfOffer.getText().isEmpty()) {
            nameOfOffer.clear();
        }

        if(!city.getText().isEmpty()) {
            city.clear();
        }

        if(!minExpirience.getText().isEmpty()) {
            minExpirience.clear();
        }

        if(!minSalary.getText().isEmpty()) {
            minSalary.clear();
        }

        partOfVacancy.setValue(new String("None"));
        typeOfEducation.setValue(new String("None"));
        typeOfJob.setValue(new String("None"));
        field.setValue(new String("None"));
        typeOfContract.setValue(new String("None"));
        for(ChoiceBox<String> levelsforLanguage : levelsChoiceBoxList) {
            levelsforLanguage.setValue(new String("Not Required"));
        }

    }

    public static Company getCompany() {
        return company;
    }

    public static void setCompany(Company company) {
        JobOfferCreatingController.company = company;
    }

    public static void main(String[] args) {
        launch(args);
    }
}

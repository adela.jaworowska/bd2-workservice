package sample;

import database.*;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import org.controlsfx.control.CheckTreeView;

public class JobOfferSearchingControler extends Application{
    private static JobOfferSearchingControler instance;
    private static JobOffersFilter jobOffersFilter;
    private static User user;

    @FXML
    private Button btnSearch;

    @FXML
    private TextField nameField;

    @FXML
    private TextField City;

    @FXML
    private TextField Salary;

    @FXML
    private ScrollPane Offers;

    @FXML
    private AnchorPane Filters;

    public JobOfferSearchingControler(){
        instance = this;
    }

    public static JobOfferSearchingControler getInstance() {
        return instance;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/java/sample/jobSearching.fxml").toURL(); // todo: change that path
        Parent root = new SplitPane();
        try{
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        primaryStage.setTitle("Searching");
        primaryStage.setScene(new Scene(root));
        //primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public void createPage(Pane jobSearching) throws MalformedURLException {
        URL url = new File( "src/main/java/sample/jobSearching.fxml").toURL();
        try {
            jobSearching = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFiterWindow() throws SQLException {
        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Filters");
        CheckBoxTreeItem<String> partOfVacancy = new CheckBoxTreeItem<String>("Part of vacancy");
        CheckBoxTreeItem<String> typeOfContract = new CheckBoxTreeItem<String>("Type of contract");
        CheckBoxTreeItem<String> typeOfJob = new CheckBoxTreeItem<String>("Type of job");
        CheckBoxTreeItem<String> typeOfEducation = new CheckBoxTreeItem<String>("Type of education");
        CheckBoxTreeItem<String> field = new CheckBoxTreeItem<String>("Field");
        CheckBoxTreeItem<String> language = new CheckBoxTreeItem<String>("Language");

        DAOManager dm = DAOManager.getInstance();
        try {
            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            ArrayList<PartOfVacancy> partOfVacancies = partOfVacancyDAO.getAllPartsOfVacancies();

            for(int i = 0; i < partOfVacancies.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> partOfVacancyItem = new CheckBoxTreeItem<String>(partOfVacancies.get(i).getName());

                partOfVacancyItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                        if(partOfVacancyItem.isSelected()) {
                            jobOffersFilter.addIdPartOfVacancy(partOfVacancies.get(finalI).getIdPartOfVacancy());
                        } else {
                            jobOffersFilter.removeIdPartOfVacancy(partOfVacancies.get(finalI).getIdPartOfVacancy());
                        }
                });
                partOfVacancy.getChildren().add(partOfVacancyItem);
            }

            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            ArrayList<TypeOfContract> typeOfContracts = typeOfContractDAO.getAllTypesOfContracts();

            for(int i = 0; i < typeOfContracts.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> typeOfContractItem = new CheckBoxTreeItem<String>(typeOfContracts.get(i).getName());

                typeOfContractItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                    if(typeOfContractItem.isSelected()) {
                        jobOffersFilter.addIdTypeOfConcract(typeOfContracts.get(finalI).getIdTypeOfContract());
                    } else {
                        jobOffersFilter.removeIdTypeOfConcract(typeOfContracts.get(finalI).getIdTypeOfContract());
                    }
                });
                typeOfContract.getChildren().add(typeOfContractItem);
            }

            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            ArrayList<TypeOfJob> typeOfJobs = typeOfJobDAO.getAllTypesOfJobs();

            for(int i = 0; i < typeOfJobs.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> typeOfJobItem = new CheckBoxTreeItem<String>(typeOfJobs.get(i).getName());

                typeOfJobItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                    if(typeOfJobItem.isSelected()) {
                        jobOffersFilter.addIdTypeOfJob(typeOfJobs.get(finalI).getIdTypeOfJob());
                    } else {
                        jobOffersFilter.removeIdTypeOfJob(typeOfJobs.get(finalI).getIdTypeOfJob());
                    }
                });
                typeOfJob.getChildren().add(typeOfJobItem);
            }

            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            ArrayList<TypeOfEducation> typeOfEducations = typeOfEducationDAO.getAllTypesOfEducations();

            for(int i = 0; i < typeOfEducations.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> typeOfEducationItem = new CheckBoxTreeItem<String>(typeOfEducations.get(i).getName());

                typeOfEducationItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                    if(typeOfEducationItem.isSelected()) {
                        jobOffersFilter.addIdTypeOfEducation(typeOfEducations.get(finalI).getIdTypeOfEducation());
                    } else {
                        jobOffersFilter.removeIdTypeOfEducation(typeOfEducations.get(finalI).getIdTypeOfEducation());
                    }
                });
                typeOfEducation.getChildren().add(typeOfEducationItem);
            }

            FieldDAO fieldDAO = dm.getFieldDAO();
            ArrayList<Field> fields = fieldDAO.getAllFields();

            for(int i = 0; i < fields.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> fieldItem = new CheckBoxTreeItem<String>(fields.get(i).getName());

                fieldItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                    if(fieldItem.isSelected()) {
                        jobOffersFilter.addIdField(fields.get(finalI).getIdField());
                    } else {
                        jobOffersFilter.removeIdField(fields.get(finalI).getIdField());
                    }
                });
                field.getChildren().add(fieldItem);
            }

            LanguageDAO languageDAO = dm.getLanguageDAO();
            ArrayList<Language> languages = languageDAO.getAllLanguages();

            for(int i = 0; i < languages.size(); ++i) {

                final int finalI = i;
                CheckBoxTreeItem<String> languageItem = new CheckBoxTreeItem<String>(languages.get(i).getName());

                languageItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                    if(languageItem.isSelected()) {
                        jobOffersFilter.addIdLanguage(languages.get(finalI).getIdLanguage());
                    } else {
                        jobOffersFilter.removeIdLanguage(languages.get(finalI).getIdLanguage());
                    }
                });
                language.getChildren().add(languageItem);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        root.getChildren().addAll(partOfVacancy, typeOfContract, typeOfEducation, typeOfJob, field, language);


        CheckTreeView checkTreeView = new CheckTreeView(root);
        checkTreeView.setMaxSize(Filters.getMaxWidth(), Filters.getMaxHeight());
        try{
            Filters.getChildren().add(checkTreeView);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void createOfferWindow() {
        DAOManager dm = DAOManager.getInstance();
        try{
            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            ArrayList<JobOffer> jobOffers = jobOfferDAO.getJobOfferListByJobOfferFilter(jobOffersFilter);
            VBox vbox = new VBox();
            vbox.setPadding(new Insets(10, 50, 50, 50));
            vbox.setSpacing(10);
            for(int i = 0; i < jobOffers.size(); ++i) {
                Label result = new Label(new Integer(i + 1).toString() + ". " + jobOffers.get(i).toString());

                final int finalI = i;
                result.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        showOfferDetails(jobOffers.get(finalI));
                    }
                });

                vbox.getChildren().add(result);
            }

            Offers.setContent(vbox);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getNameOfOfferText() {
        return nameField.getText();
    }

    @FXML
    private void btnSearchAction(){
        jobOffersFilter.setNameOfOffer(getNameOfOfferText());
        if(!Salary.getText().isEmpty()) {
            jobOffersFilter.setMinSalary(Float.parseFloat(Salary.getText()));
        } else {
            jobOffersFilter.setMinSalary(0);
        }

        if(!City.getText().isEmpty()) {
            jobOffersFilter.setCity(City.getText());
        }
        else {
            jobOffersFilter.setCity(new String());
        }

        createOfferWindow();
    }

    private void showOfferDetails(JobOffer jobOffer) {
        GridPane gridPane = new GridPane();
        VBox vbox = new VBox();

        DAOManager dm = DAOManager.getInstance();

        try {
            CompanyDAO companyDAO = dm.getCompanyDAO();
            Company company = companyDAO.getCompanyByID(jobOffer.getIdCompany());
            Label offeraName = new Label(jobOffer.getnameOfOffer());
            Label companyName = new Label("Company: " + company.getName());
            vbox.getChildren().add(companyName);

            int numberOfRowSpan = 0;

            if(jobOffer.getMinExpirience() != 0) {
                Label minExpirience = new Label("Min Expirience: " + jobOffer.getMinExpirience());
                vbox.getChildren().add(minExpirience);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdPartOfVacancy() != 0) {
                PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
                PartOfVacancy partOfVacancy = partOfVacancyDAO.getPartOfVacancyById(jobOffer.getIdPartOfVacancy());
                Label partOfVacancyLabel = new Label("Part of vacancy: " + partOfVacancy.getName());
                vbox.getChildren().add(partOfVacancyLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfEducation() != 0) {
                TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
                TypeOfEducation typeOfEducation = typeOfEducationDAO.getTypeOfEducationById(jobOffer.getIdTypeOfEducation());
                Label typeOfEducationLabel = new Label("Type of Education: " + typeOfEducation.getName());
                vbox.getChildren().add(typeOfEducationLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfConcract() != 0) {
                TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
                TypeOfContract typeOfContract = typeOfContractDAO.getTypeOfContractById(jobOffer.getIdTypeOfConcract());
                Label typeOfContractLabel = new Label("Type of contract: " + typeOfContract.getName());
                vbox.getChildren().add(typeOfContractLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfJob() != 0) {
                TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
                TypeOfJob typeOfJob = typeOfJobDAO.getTypeOfJobById(jobOffer.getIdTypeOfJob());
                Label typeOfJobLabel = new Label("Type of job: " + typeOfJob.getName());
                vbox.getChildren().add(typeOfJobLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getCity() != null) {
                Label city = new Label("City: " + jobOffer.getCity());
                vbox.getChildren().add(city);
                ++numberOfRowSpan;
            }

            if(jobOffer.getMinSalary() != 0) {
                Label salary = new Label("Salary: " + jobOffer.getMinSalary());
                vbox.getChildren().add(salary);
                ++numberOfRowSpan;
            }

            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            if(!requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer()).isEmpty()) {
                HBox requiredLanguages = new HBox();
                VBox requiredLanguagesList = new VBox();
                Label requiredLanguagesLabel = new Label("Required languages: ");

                for(RequiredLanguage requiredLanguage : requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer())) {
                    Label requiredLanguageLabel = new Label(requiredLanguage.toString());
                    requiredLanguagesList.getChildren().add(requiredLanguageLabel);
                    ++numberOfRowSpan;
                }

                vbox.getChildren().add(requiredLanguages);
                requiredLanguages.getChildren().addAll(requiredLanguagesLabel, requiredLanguagesList);
            }

            Label info = new Label();
            if(SignInLogic.isUserSignedIn){
                user = SignInLogic.loggedUser;
            }

            Button applay = new Button("Applay");
            applay.setOnAction(new EventHandler<ActionEvent>() {

                @Override public void handle(ActionEvent e) {
                    if(user != null) {
                        try{
                            AplicationDAO aplicationDAO = dm.getAplicationDAO();
                            JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
                            Jobseeker jobseeker = jobseekerDAO.getJobseekerByUserId(user.getIdUser());
                            if(!aplicationDAO.isAplicationExistByJobseekerIdAndJobOfferId(jobseeker.getIdJobseeker(), jobOffer.getIdJobOffer())) {
                                java.util.Date utilDate = new java.util.Date();
                                Aplication aplication = new Aplication(new Date(utilDate.getTime()), jobOffer.getIdJobOffer(), jobseeker.getIdJobseeker());
                                aplicationDAO.create(aplication);
                                info.setText("Applied");
                                MyAplicationsController mAC = MyAplicationsController.getInstance();
                                mAC.initialize();
                            } else {
                                info.setText("You have already applied for this offer");
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                    else {
                        info.setText("You are not logged");
                    }
                }
            });

            Button back = new Button("Back");
            back.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    createOfferWindow();
                }
            });


            gridPane.add(offeraName, 2, 1);
            gridPane.add(vbox, 3, 3, 2, numberOfRowSpan);
            gridPane.add(applay,5, 30);
            gridPane.add(back,1,30);
            gridPane.add(info, 3, 35);

            Offers.setContent(gridPane);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    public void initialize() {
        jobOffersFilter = new JobOffersFilter();
        try{
            createFiterWindow();
            createOfferWindow();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setFilter(String city, String nameOfOffer)
    {
        jobOffersFilter.setCity(city);
        jobOffersFilter.setNameOfOffer(nameOfOffer);
        this.City.setText(city);
        this.nameField.setText(nameOfOffer);
        createOfferWindow();

    }


}

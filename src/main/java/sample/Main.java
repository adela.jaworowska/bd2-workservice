package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.io.File;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        URL url = new File("src/main/java/sample/sample.fxml").toURL();
        Parent root = new GridPane();
        try{
            //root = FXMLLoader.load(getClass().getResource("sample.fxml"));
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        primaryStage.setTitle("Work Service");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}


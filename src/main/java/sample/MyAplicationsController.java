package sample;

import database.*;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.ListView;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

public class MyAplicationsController extends Application {
    private static MyAplicationsController instance;
    private static Jobseeker jobseeker;
    private ArrayList<Integer> aplicationStatusesList;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private ListView<HBox> aplicationsListView;

    @FXML
    private TextField nameOfOfferTextField;

    @FXML
    private CheckBox checkBox1;

    @FXML
    private CheckBox checkBox2;

    @FXML
    private CheckBox checkBox3;

    @FXML
    private CheckBox checkBox4;

    @FXML
    private AnchorPane anchorPane;

    public MyAplicationsController() {
        instance = this;
    }

    public static MyAplicationsController getInstance() {
        return instance;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/java/sample/myAplications.fxml").toURL(); // todo: change that path
        Parent root = new AnchorPane();
        try{
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        primaryStage.setTitle("MyApplications");
        primaryStage.setScene(new Scene(root));
        //primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public void createPage(Pane aplcationResponse) throws MalformedURLException {
        URL url = new File( "src/main/java/sample/myAplications.fxml").toURL();
        try {
            aplcationResponse = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        aplicationStatusesList = new ArrayList<>();
        DAOManager dm = DAOManager.getInstance();
        try{
            JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
            Jobseeker jobseeker1 = jobseekerDAO.getJobseekerByUserId(SignInLogic.loggedUser.getIdUser());
            setJobseeker(jobseeker1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        createMyAplicationsWindow();
    }

    private void createMyAplicationsWindow() {
        DAOManager dm = DAOManager.getInstance();
        try {
            AplicationStatusDAO aplicationStatusDAO = dm.getAplicationStatusDAO();
            ArrayList<AplicationStatus> aplicationStatuses = aplicationStatusDAO.getAllAplicationStatuses();

            checkBox1.setText(aplicationStatuses.get(0).getName());
            checkBox2.setText(aplicationStatuses.get(1).getName());
            checkBox3.setText(aplicationStatuses.get(2).getName());
            checkBox4.setText(aplicationStatuses.get(3).getName());

            checkBox1.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(checkBox1.isSelected()) {
                        aplicationStatusesList.add(new Integer(1));
                    } else {
                        aplicationStatusesList.remove(new Integer(1));
                    }
                }
            });

            checkBox2.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(newValue) {
                        aplicationStatusesList.add(new Integer(2));
                    } else {
                        aplicationStatusesList.remove(new Integer(2));
                    }
                }
            });

            checkBox3.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(newValue) {
                        aplicationStatusesList.add(new Integer(3));
                    } else {
                        aplicationStatusesList.remove(new Integer(3));
                    }
                }
            });

            checkBox4.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(newValue) {
                        aplicationStatusesList.add(new Integer(4));
                    } else {
                        aplicationStatusesList.remove(new Integer(4));
                    }
                }
            });
            checkBox1.setSelected(true);
            checkBox2.setSelected(true);
            checkBox3.setSelected(true);
            checkBox4.setSelected(true);

            search();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void btnSearchAction(){
        search();
    }

    @FXML
    public void btnEraseAction(){
        erase();
    }

    private void search() {
        scrollPane.setContent(anchorPane);
        aplicationsListView.getItems().clear();
        DAOManager dm = DAOManager.getInstance();
        try {
            AplicationDAO aplicationDAO = dm.getAplicationDAO();
            ArrayList<Aplication> aplications = aplicationDAO.getAplicationsByJobseekerIdAndAplicationStatuseIds(jobseeker.getIdJobseeker(), aplicationStatusesList);

            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            AplicationStatusDAO aplicationStatusDAO = dm.getAplicationStatusDAO();
            for(Aplication aplication : aplications) {
                JobOffer jobOffer = jobOfferDAO.getJobOfferByID(aplication.getIdJobOffer());
                AplicationStatus aplicationStatus = aplicationStatusDAO.getAplicationStatusById(aplication.getIdAplicationStatus());
                HBox hBox = new HBox();
                if(jobOffer.getnameOfOffer().matches(".*" + nameOfOfferTextField.getText() + ".*")) {
                    Label aplcationLabel = new Label(jobOffer.getnameOfOffer() + "     " + aplicationStatus.getName() + "     ");
                    Button deleteBtn = new Button("DELETE");
                    hBox.getChildren().add(aplcationLabel);
                    hBox.getChildren().add(deleteBtn);
                    hBox.setSpacing(100.0);
                    aplicationsListView.getItems().add(hBox);

                    deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent e) {
                            try {
                                aplicationDAO.deleteAplicationById(aplication.getIdAplication());
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                            search();
                        }
                    });

                    aplcationLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent e) {
                            if(e.getClickCount() == 2){
                                showOfferDetails(jobOffer);
                            }
                        }
                    });
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showOfferDetails(JobOffer jobOffer) {
        GridPane gridPane = new GridPane();
        VBox vbox = new VBox();

        DAOManager dm = DAOManager.getInstance();

        try {
            Label offeraName = new Label(jobOffer.getnameOfOffer());
            CompanyDAO companyDAO = dm.getCompanyDAO();
            Company company = companyDAO.getCompanyByID(jobOffer.getIdCompany());
            Label companyName = new Label("Company: " + company.getName());
            vbox.getChildren().add(companyName);

            int numberOfRowSpan = 0;

            if (jobOffer.getMinExpirience() != 0) {
                Label minExpirience = new Label("Min Expirience: " + jobOffer.getMinExpirience());
                vbox.getChildren().add(minExpirience);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdPartOfVacancy() != 0) {
                PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
                PartOfVacancy partOfVacancy = partOfVacancyDAO.getPartOfVacancyById(jobOffer.getIdPartOfVacancy());
                Label partOfVacancyLabel = new Label("Part of vacancy: " + partOfVacancy.getName());
                vbox.getChildren().add(partOfVacancyLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfEducation() != 0) {
                TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
                TypeOfEducation typeOfEducation = typeOfEducationDAO.getTypeOfEducationById(jobOffer.getIdTypeOfEducation());
                Label typeOfEducationLabel = new Label("Type of Education: " + typeOfEducation.getName());
                vbox.getChildren().add(typeOfEducationLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfConcract() != 0) {
                TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
                TypeOfContract typeOfContract = typeOfContractDAO.getTypeOfContractById(jobOffer.getIdTypeOfConcract());
                Label typeOfContractLabel = new Label("Type of contract: " + typeOfContract.getName());
                vbox.getChildren().add(typeOfContractLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getIdTypeOfJob() != 0) {
                TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
                TypeOfJob typeOfJob = typeOfJobDAO.getTypeOfJobById(jobOffer.getIdTypeOfJob());
                Label typeOfJobLabel = new Label("Type of job: " + typeOfJob.getName());
                vbox.getChildren().add(typeOfJobLabel);
                ++numberOfRowSpan;
            }

            if (jobOffer.getCity() != null) {
                Label city = new Label("City: " + jobOffer.getCity());
                vbox.getChildren().add(city);
                ++numberOfRowSpan;
            }

            if (jobOffer.getMinSalary() != 0) {
                Label salary = new Label("Salary: " + jobOffer.getMinSalary());
                vbox.getChildren().add(salary);
                ++numberOfRowSpan;
            }

            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            if (!requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer()).isEmpty()) {
                HBox requiredLanguages = new HBox();
                VBox requiredLanguagesList = new VBox();
                Label requiredLanguagesLabel = new Label("Required languages: ");
                for (RequiredLanguage requiredLanguage : requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer())) {
                    Label requiredLanguageLabel = new Label(requiredLanguage.toString());
                    requiredLanguagesList.getChildren().add(requiredLanguageLabel);
                    ++numberOfRowSpan;
                }

                vbox.getChildren().add(requiredLanguages);
                requiredLanguages.getChildren().addAll(requiredLanguagesLabel, requiredLanguagesList);
            }

            Button back = new Button("Back");

            back.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    search();
                }
            });

            gridPane.add(offeraName, 2, 1);
            gridPane.add(vbox, 3, 3, 2, numberOfRowSpan);
            gridPane.add(back,1,30);

            scrollPane.setContent(gridPane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void erase() {
        checkBox1.setSelected(true);
        checkBox2.setSelected(true);
        checkBox3.setSelected(true);
        checkBox4.setSelected(true);
        nameOfOfferTextField.clear();

        search();

    }

    public static Jobseeker getJobseeker() {
        return jobseeker;
    }

    public static void setJobseeker(Jobseeker jobseeker) {
        MyAplicationsController.jobseeker = jobseeker;
    }

}

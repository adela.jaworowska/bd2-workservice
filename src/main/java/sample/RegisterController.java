package sample;

import database.DAOManager;
import database.UserDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import logic.RegisterLogic;
import logic.SignInLogic;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;


public class RegisterController{


    RegisterLogic registerLogic = new RegisterLogic();
    DAOManager dm = DAOManager.getInstance();

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnRegister;

    @FXML
    private CheckBox checkBoxIsCompany;

    @FXML
    private CheckBox checkBoxAgree;

    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField textFieldName;

    @FXML
    private TextField textFieldLastName;

    @FXML
    private TextField textFieldLogin;

    @FXML
    private TextField textFieldPassword;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private TextField textFieldCompanyName;

    @FXML
    private TextField textFieldNIP;

    @FXML
    private TextField textFieldKRS;

    @FXML
    private TextField textFieldPhoneNumber;

    @FXML
    private Label failToFill;

    @FXML
    private Label failToCheck;

    @FXML
    private Label labelIncorrectData;

    @FXML
    private Label labelOtherLogin;

    @FXML
    private Label labelThanks;

    @FXML
    private Label oblName, oblLastName, oblLogin, oblPassword, oblEmail, oblCompanyName, oblNIP, oblKRS, oblPhoneNumb, oblDate, oblAgree;

    @FXML
    private Label alertNIP, alertKRS, alertPhone;

    private static RegisterController instance;

    public RegisterController(){
        instance = this;
    }

    public static RegisterController getInstance(){
        return instance;
    }

    @FXML
    public void initialize(){
        resetAllAlerts();
        datePicker.setValue(LocalDate.of(2000,1,1));
    }

    @FXML
    private void BtnCancelAction() throws SQLException {
        resetAll();

    }

    @FXML
    private void BtnRegisterAction() throws SQLException {
        resetAllAlerts();
        UserDAO userDAO = dm.getUserDAO();

        if(!isAllDataEntered()){
            failToFill.setVisible(true);
            return;
        }
        if(userDAO.loginExists(getLogin())) {
            labelOtherLogin.setVisible(true);
            return;
        }
        if(!isAgreement()) {
            failToCheck.setVisible(true);
            return;
        }
        if(isCompany()){
            if(registerLogic.isCompanyDataCorrect(getLogin(), getPassword(), getEmail(), getCompanyName(), getNIP(), getKRS(), getPhoneNumber()))
                registerLogic.registerCompany(getLogin(), getPassword(), getEmail(), getCompanyName(), getNIP(), getKRS(), getPhoneNumber());
            else{
                labelIncorrectData.setVisible(true);
                return;
            }
        }
        else
            registerLogic.registerPerson(getName(), getLastName(), getLogin(), getPassword(), getEmail(), getDate());

        MainViewController mainController = MainViewController.getInstance();
        mainController.initPanes();

        resetAll();
        labelThanks.setVisible(true);
    }

    public void CheckBoxCompany(ActionEvent event){
        if(checkBoxIsCompany.isSelected()){
            oblName.setVisible(false);
            oblLastName.setVisible(false);
            oblDate.setVisible(false);
        }
        else{
            oblName.setVisible(true);
            oblLastName.setVisible(true);
            oblDate.setVisible(true);
        }
    }

    public void CheckBoxAgree(ActionEvent event){
        if(checkBoxAgree.isSelected()){
            failToCheck.setVisible(false);
        }
    }

    public void ActionNIP(ActionEvent event){
        System.out.println("ACTION NIP ");
        if(!registerLogic.isNIPcorrect(getNIP())){
            alertNIP.setVisible(true);
        }
        else
            alertNIP.setVisible(false);
    }

    public void ActionKRS(ActionEvent event){
        if(!registerLogic.isKRScorrect(getKRS())){
            alertKRS.setVisible(true);
        }
        else
            alertKRS.setVisible(false);
    }

    public void ActionPhone(ActionEvent event){
        if(!registerLogic.isPhoneNumberCorrect(getPhoneNumber())){
            alertPhone.setVisible(true);
        }
        else
            alertPhone.setVisible(false);
    }

    private boolean isAllDataEntered(){
        if(textFieldLogin.getText().equals("") || textFieldPassword.getText().equals("") || textFieldEmail.getText().equals(""))
            return false;

        if(isCompany()){
            if(textFieldCompanyName.getText().equals("") || textFieldNIP.getText().equals("") || textFieldKRS.getText().equals("") ||textFieldPhoneNumber.getText().equals(""))
                return false;
        }
        if(!isCompany()){
            if(textFieldName.getText().equals("") || textFieldLastName.getText().equals(""))
                return false;
        }
        return true;
    }

    private String getName(){
        return textFieldName.getText();
    }

    private String getLastName(){
        return textFieldLastName.getText();
    }

    private String getLogin(){
        return textFieldLogin.getText();
    }

    private String getPassword(){
        return textFieldPassword.getText();
    }

    private String getEmail(){
        return textFieldEmail.getText();
    }

    private String getCompanyName(){
        return textFieldCompanyName.getText();
    }

    private String getNIP(){
        return textFieldNIP.getText();
    }

    private String getKRS(){
        return textFieldKRS.getText();
    }

    private String getPhoneNumber(){
        return textFieldPhoneNumber.getText();
    }

    private boolean isCompany(){
        return checkBoxIsCompany.isSelected();
    }

    private boolean isAgreement(){
        return checkBoxAgree.isSelected();
    }

    private Date getDate(){
        return Date.valueOf(datePicker.getValue());
    }

    private void resetAllAlerts(){
        failToCheck.setVisible(false);
        failToFill.setVisible(false);
        labelIncorrectData.setVisible(false);
        alertPhone.setVisible(false);
        alertKRS.setVisible(false);
        alertNIP.setVisible(false);
        labelThanks.setVisible(false);
        labelOtherLogin.setVisible(false);
    }

    private void resetAll(){
        resetAllAlerts();
        textFieldName.clear();
        textFieldLastName.clear();
        textFieldLogin.clear();
        textFieldPassword.clear();
        textFieldEmail.clear();
        textFieldCompanyName.clear();
        textFieldNIP.clear();
        textFieldKRS.clear();
        textFieldPhoneNumber.clear();
        checkBoxAgree.setSelected(false);
        checkBoxIsCompany.setSelected(false);
    }



}

package sample;


import database.DAOManager;
import database.UserDAO;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import logic.SignInLogic;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.File;
import java.sql.SQLException;


public class SignInController {

    public UserDAO userDAO;
    public SignInLogic signInLogic;

    // does not work
    public void createPage(Pane pane) throws MalformedURLException {
        URL url;
        try {
            url = new File( "src/main/java/sample/signIn.fxml").toURL();
            Pane newPane = FXMLLoader.load(url);
            pane.getChildren().setAll(newPane);
            //signIn = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private BorderPane signInBoarderPane;

    @FXML
    private Label SignInLabel;

    @FXML
    private Label failLabel;

    @FXML
    private Label signedInLabel;

    @FXML
    private Button btnSignIn;

    @FXML
    private Button btnRegister;

    @FXML
    private Button btnCancel;

    @FXML
    private TextField textFieldUsername;

    @FXML
    private PasswordField passwordField;

    private Stage primaryStage;

    BorderPane signIn;

    private static SignInController instance;

    public SignInController(){
        instance = this;
    }

    public static SignInController getInstance() {
        return instance;
    }

    @FXML
    public void initialize(){
        failLabel.setVisible(false);
        signedInLabel.setVisible(false);
    }

    @FXML
    private void BtnSignInAction() throws SQLException {
        DAOManager dm = DAOManager.getInstance();
        UserDAO userDAO = dm.getUserDAO();
        System.out.println("SignIn clicked");
        if (!userDAO.isUser(getUsernameFieldText(), getPasswordFieldText())){
            failLabel.setVisible(true);
            System.out.println(getPasswordFieldText());
            System.out.println(signInLogic.hashFunc(getPasswordFieldText()));
        }
        else {
            System.out.println(getPasswordFieldText());
            System.out.println("OK");
            signedInLabel.setVisible(true);
            failLabel.setVisible(false);

            int id = userDAO.getUserId(getUsernameFieldText(), getPasswordFieldText());
            logic.User user = userDAO.getUserByID(id);

            signInLogic.setUserAsLoggedIn(user, getUsernameFieldText());

        }
    }

    @FXML
    private void BtnRegisterAction(){
        System.out.println("Register clicked");
        URL url = null;
        MainViewController mainController = MainViewController.getInstance();
        try {
            url = new File("src/main/java/sample/register.fxml").toURL();
            Pane register = FXMLLoader.load(url);
            mainController.getPaneMyAccount().getChildren().setAll(register);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void BtnCancelAction(){
        System.out.println("Cancel clicked");
        clearUsernameField();
        clearPasswordField();
        failLabel.setVisible(false);
        signedInLabel.setVisible(false);
    }


    private String getUsernameFieldText(){
        return textFieldUsername.getText();
    }

    private String getPasswordFieldText(){
        return passwordField.getText();
    }

    private void clearUsernameField(){
        textFieldUsername.clear();
    }

    private void clearPasswordField(){ passwordField.clear(); }

}


package sample;


import database.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

public class EditProfileController {

    EditProfileLogic editProfileLogic = new EditProfileLogic();
    @FXML
    ChoiceBox<String> cbLanguage = new ChoiceBox<>();
    @FXML
    ChoiceBox<String> cbLevel = new ChoiceBox<>();;
    @FXML
    ChoiceBox<String> cbField = new ChoiceBox<>();;
    @FXML
    ChoiceBox<String> cbEducationType = new ChoiceBox<>();
    @FXML
    ChoiceBox<String> cbStartYear = new ChoiceBox<>();;
    @FXML
    TextArea fieldDescription;
    @FXML
    TextArea fieldSchoolName;
    @FXML
    Button btnAddLanguage;
    @FXML
    Button btnAddExperience;
    @FXML
    Button btnAddEducation;
    @FXML
    Button btnBack;

    @FXML
    public void initialize() {
        System.out.println("initialize");
        prepareChoiceBoxes();
        cbLanguage.setValue("English");
        cbLevel.setValue("Beginner");
        cbEducationType.setValue("Primary");
        cbStartYear.setValue("2019");
        cbField.setValue("IT");

    }

    private void prepareChoiceBoxes(){
        CheckBoxTreeItem<String> language = new CheckBoxTreeItem<String>("Language");
        try{
        DAOManager dm = DAOManager.getInstance();
        LanguageDAO languageDAO = dm.getLanguageDAO();
        ArrayList<Language> languages = languageDAO.getAllLanguages();

        for (int i = 0; i < languages.size(); ++i) {
            final int finalI = i;
            CheckBoxTreeItem<String> languageItem = new CheckBoxTreeItem<String>(languages.get(i).getName());
            cbLanguage.getItems().add(languageItem.getValue());
        }

        FieldDAO fieldDAO = dm.getFieldDAO();
        ArrayList<Field> fields = fieldDAO.getAllFields();

        for (int i = 0; i < fields.size(); ++i) {

            final int finalI = i;
            CheckBoxTreeItem<String> fieldItem = new CheckBoxTreeItem<String>(fields.get(i).getName());
            cbField.getItems().add(fieldItem.getValue());
        }

        TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
        ArrayList<TypeOfEducation> types = typeOfEducationDAO.getAllTypesOfEducations();

        for (int i = 0; i < types.size(); ++i) {

            final int finalI = i;
            CheckBoxTreeItem<String> typeOfEducationItem = new CheckBoxTreeItem<String>(types.get(i).getName());
            cbEducationType.getItems().add(typeOfEducationItem.getValue());
        }

        LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            ArrayList<LanguageLevel> levels = languageLevelDAO.getAllLanguageLevels();

        for (int i = 0; i < levels.size(); ++i) {

            final int finalI = i;
            CheckBoxTreeItem<String> typeOfLanguageLevel = new CheckBoxTreeItem<String>(levels.get(i).getName());
            cbLevel.getItems().add(typeOfLanguageLevel.getValue());
        }

        for (int i = 2019; i > 1920; --i) {
            cbStartYear.getItems().add(Integer.toString(i));
        }

        } catch (SQLException e) {
        e.printStackTrace();
    }

}

    @FXML
    private void btnAddLanguageAction() throws SQLException {
        System.out.println("Add lang clicked");
        editProfileLogic.addNewKnownLanguage(cbLanguage.getValue(), cbLevel.getValue());
        System.out.println("Language added.");
    }

    @FXML
    private void btnAddExperienceAction() throws SQLException {
        System.out.println("Add exp clicked");
        editProfileLogic.addNewExperience(fieldDescription.getText(), cbField.getValue());
        fieldDescription.clear();
        System.out.println("Experience added.");
    }

    @FXML
    private void btnAddEducationAction() throws SQLException {
        System.out.println("Add education clicked");
        editProfileLogic.addNewEducation(fieldSchoolName.getText(), Integer.parseInt(cbStartYear.getValue()), cbEducationType.getValue());
        fieldSchoolName.clear();
        System.out.println("School added.");
    }

    @FXML
    private void btnBackAction() throws SQLException {
        URL url = null;
        MainViewController mainController = MainViewController.getInstance();
        try {
            url = new File("src/main/java/sample/myAccount.fxml").toURL();
            SplitPane register = FXMLLoader.load(url);
            mainController.getPaneMyAccount().getChildren().setAll(register);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}

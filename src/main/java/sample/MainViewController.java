
package sample;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import database.DAOManager;
import database.JobseekerDAO;
import database.UserDAO;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import logic.JobOffersFilter;
import logic.SignInLogic;

public class MainViewController {
    private String basicButtonStyle = "-fx-background-color: #1c2144 ; -fx-text-fill: #ffffff; -fx-background-radius: 0;";
    private String activeButtonStyle= "-fx-background-color: #2473ab; -fx-text-fill: #ffffff; -fx-background-radius: 0;";
    private boolean initialized =false;

    private UserDAO userDAO;
    private DAOManager dm;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane anchorPaneMain;

    @FXML
    private Pane paneContact;

    @FXML
    private Label labelContactInfo;

    @FXML
    private Pane paneAnalysis;

    @FXML
    private Label labelSearchInfo2;

    @FXML
    private Pane paneSearch;

    @FXML
    private Label labelSearchInfo;

    @FXML
    private Pane paneOffers;

    @FXML
    private Label labelOffers;

    @FXML
    private Pane paneMyAccount;

    @FXML
    private Label labelMyAccountInfo;

    @FXML
    private Pane paneMainPage;

    @FXML
    private AnchorPane anchorPaneTop;

    @FXML
    private ImageView imageViewLogo;

    @FXML
    private Pane paneSignIn;

    @FXML
    private Button btnSignIn;

    @FXML
    private TextField textFieldSearchPhrase;

    @FXML
    private TextField textFieldSearchLocal;

    @FXML
    private Button btnSearch;

    @FXML
    private AnchorPane anchorPaneSideMenu;

    @FXML
    private Button btnSideMenu;

    @FXML
    private Button btnSideMenu3;

    @FXML
    private Button btnSideMenu2;

    @FXML
    private Button btnSideMenu4;

    @FXML
    private Button btnSideMenu5;

    @FXML
    private Button btnSideMenu6;

    @FXML
    private SplitPane splitPaneOffers;

    public Pane getPaneMainPage() {
        return paneMainPage;
    }

    private static MainViewController instance;

    public MainViewController(){
        instance = this;
    }

    public static MainViewController getInstance() {
        return instance;
    }

    @FXML
    void initialize() throws SQLException{
        assert anchorPaneMain != null : "fx:id=\"anchorPaneMain\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneContact != null : "fx:id=\"paneContact\" was not injected: check your FXML file 'sample.fxml'.";
        assert labelContactInfo != null : "fx:id=\"labelContactInfo\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneAnalysis != null : "fx:id=\"paneAnalysis\" was not injected: check your FXML file 'sample.fxml'.";
        assert labelSearchInfo2 != null : "fx:id=\"labelSearchInfo2\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneSearch != null : "fx:id=\"paneSearch\" was not injected: check your FXML file 'sample.fxml'.";
        assert labelSearchInfo != null : "fx:id=\"labelSearchInfo\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneOffers != null : "fx:id=\"paneOffers\" was not injected: check your FXML file 'sample.fxml'.";
        assert labelOffers != null : "fx:id=\"labelOffers\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneMyAccount != null : "fx:id=\"paneMyAccount\" was not injected: check your FXML file 'sample.fxml'.";
        assert labelMyAccountInfo != null : "fx:id=\"labelMyAccountInfo\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneMainPage != null : "fx:id=\"paneMainPage\" was not injected: check your FXML file 'sample.fxml'.";
        assert anchorPaneTop != null : "fx:id=\"anchorPaneTop\" was not injected: check your FXML file 'sample.fxml'.";
        assert imageViewLogo != null : "fx:id=\"imageViewLogo\" was not injected: check your FXML file 'sample.fxml'.";
        assert paneSignIn != null : "fx:id=\"paneSignIn\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSignIn != null : "fx:id=\"btnSignIn\" was not injected: check your FXML file 'sample.fxml'.";
        assert textFieldSearchPhrase != null : "fx:id=\"textFieldSearchPhrase\" was not injected: check your FXML file 'sample.fxml'.";
        assert textFieldSearchLocal != null : "fx:id=\"textFieldSearchLocal\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSearch != null : "fx:id=\"btnSearch\" was not injected: check your FXML file 'sample.fxml'.";
        assert anchorPaneSideMenu != null : "fx:id=\"anchorPaneSideMenu\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu != null : "fx:id=\"btnSideMenu\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu3 != null : "fx:id=\"btnSideMenu3\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu2 != null : "fx:id=\"btnSideMenu2\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu4 != null : "fx:id=\"btnSideMenu4\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu5 != null : "fx:id=\"btnSideMenu5\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSideMenu6 != null : "fx:id=\"btnSideMenu6\" was not injected: check your FXML file 'sample.fxml'.";

        dm = DAOManager.getInstance();
        userDAO = dm.getUserDAO();

        //OnMouseEntered, OnMouseExited
        //main page
            btnSideMenu.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu.setStyle(activeButtonStyle);
                }
            });
            btnSideMenu.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu.setStyle(basicButtonStyle);
                }
            });
            //search
            btnSideMenu2.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu2.setStyle(activeButtonStyle);
                }
            });
            btnSideMenu2.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu2.setStyle(basicButtonStyle);
                }
            });
            //Offers
            btnSideMenu3.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu3.setStyle(activeButtonStyle);
                }
            });
            btnSideMenu3.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu3.setStyle(basicButtonStyle);
                }
            });
            //My Account
            btnSideMenu4.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu4.setStyle(activeButtonStyle);
                }
            });
            btnSideMenu4.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu4.setStyle(basicButtonStyle);
                }
            });
            //Analysis
            btnSideMenu5.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu5.setStyle(activeButtonStyle);
                }
            });
            btnSideMenu5.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    btnSideMenu5.setStyle(basicButtonStyle);
                }
            });
        //Contact
        btnSideMenu6.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                btnSideMenu6.setStyle(activeButtonStyle);
            }
        });
        btnSideMenu6.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                btnSideMenu6.setStyle(basicButtonStyle);
            }
        });

        //switch between panes
        //main page
        btnSideMenu.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paneMainPage.toFront();
            }
        });
        //search
        btnSideMenu2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(!initialized)
                    initPanes();
                paneSearch.toFront();
            }
        });
        //offers
        btnSideMenu3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if(!initialized)
                    initPanes();

                if(!SignInLogic.isUserSignedIn)
                    paneMyAccount.toFront();
                else paneOffers.toFront();
            }
        });
        //my account
        btnSideMenu4.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paneMyAccount.toFront();
            }
        });
        btnSideMenu5.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paneAnalysis.toFront();
            }
        });
        btnSideMenu6.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                paneContact.toFront();
            }
        });

        btnSearch.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(!initialized)
                    initPanes();
                String searchText = textFieldSearchPhrase.getText();
                String searchLocal = textFieldSearchLocal.getText();

                JobOfferSearchingControler jobOfferSearchingControler = JobOfferSearchingControler.getInstance();
                jobOfferSearchingControler.setFilter(searchLocal, searchText);

                paneSearch.toFront();
            }
        });

        // shows login pane
        btnSignIn.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!initialized)
                    initPanes();
                paneMyAccount.toFront();
            }
        });



        }
        public void changeToSignedUser() throws SQLException
        {
            btnSignIn.setText(SignInLogic.loginFirst);
            paneSignIn.setStyle("-fx-background-color: #1c2144; -fx-border-color: #1c2144; -fx-border-width: 2;");
            btnSignIn.setStyle("-fx-background-color: #eca39D; -fx-background-radius: 30; -fx-text-fill: #ffffff;");

            Label labelName = new Label();
            labelName.setText(SignInLogic.getUserName());

            boolean isCompany = true;

            JobseekerDAO jsDAO = dm.getJobseekerDAO();
            if(jsDAO.isJobSeeker((SignInLogic.loggedUser.getIdUser())))
                isCompany = false;

            if(isCompany) {
                btnSideMenu2.setText("Add Offer");
                URL url;

                try {
                    url = new File("src/main/java/sample/jobOfferEditing.fxml").toURL();
                    ScrollPane scrollPane = FXMLLoader.load(url);
                    paneOffers.getChildren().setAll(scrollPane);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    url = new File("src/main/java/sample/jobOfferCreating.fxml").toURL();
                    ScrollPane scrollPane = FXMLLoader.load(url);
                    paneSearch.getChildren().setAll(scrollPane);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    url = new File("src/main/java/sample/aplicationResponse.fxml").toURL();
                    SplitPane splitPane = FXMLLoader.load(url);
                    paneMyAccount.getChildren().setAll(splitPane);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                btnSideMenu4.setText("My Applications");
            }
            else{
                try {
                    URL url = new File("src/main/java/sample/myAccount.fxml").toURL();
                    SplitPane scrollPane = FXMLLoader.load(url);
                    paneMyAccount.getChildren().setAll(scrollPane);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                btnSideMenu3.setText("My Applications");
                try {
                    URL url = new File("src/main/java/sample/myAplications.fxml").toURL();
                    SplitPane scrollPane = FXMLLoader.load(url);
                    paneOffers.getChildren().setAll(scrollPane);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }

        public void initPanes(){
        initialized = true;

        //offers pane
        System.out.println("offers clicked. ");
        URL url = null;
        try {
            url = new File("src/main/java/sample/jobSearching.fxml").toURL();
            splitPaneOffers = FXMLLoader.load(url);
            paneSearch.getChildren().setAll(splitPaneOffers);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        //sign in
            System.out.println("SingIn clicked. ");
            try {
                url = new File("src/main/java/sample/signIn.fxml").toURL();
                Pane signIn = FXMLLoader.load(url);
                paneMyAccount.getChildren().setAll(signIn);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


    }

    public Pane getPaneMyAccount()
    {
        return paneMyAccount;
    }
}



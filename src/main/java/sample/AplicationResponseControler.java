package sample;

import database.*;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import logic.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.controlsfx.control.CheckTreeView;

public class AplicationResponseControler extends Application {
    private static AplicationResponseControler instance;
    private static Company company;
    private static Map<Integer, ArrayList<Integer>> jobOfferIdAplicationStatusIdMap = new HashMap<>();
    private static CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Offers");

    @FXML
    private ListView aplicationListView;

    @FXML
    private AnchorPane jobOfferPane;

    @FXML
    private Button btnSearch;

    @FXML
    private Button btnErase;

    @FXML
    private ScrollPane aplicationsScrollPane;

    @FXML
    private AnchorPane aplicationAnchorPane;

    public static AplicationResponseControler getInstance() {
        return instance;
    }

    public AplicationResponseControler() {
        instance = this;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/java/sample/aplicationResponse.fxml").toURL(); // todo: change that path
        Parent root = new AnchorPane();
        try{
            root = FXMLLoader.load(url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        primaryStage.setTitle("Response");
        primaryStage.setScene(new Scene(root));
        //primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public void createPage(Pane aplcationResponse) throws MalformedURLException {
        URL url = new File( "src/main/java/sample/aplicationResponse.fxml").toURL();
        try {
            aplcationResponse = FXMLLoader.load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {
        DAOManager dm = DAOManager.getInstance();
        try {
            CompanyDAO companyDAO = dm.getCompanyDAO();
            int companyID = companyDAO.getCompanyIDByUserID(SignInLogic.loggedUser.getIdUser());
            company = companyDAO.getCompanyByID(companyID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        createAplicationresponseWindow();
    }

    private void createAplicationresponseWindow() {
        ArrayList<CheckBoxTreeItem<String>> jobOffersList = new ArrayList<>();

        DAOManager dm = DAOManager.getInstance();
        try{
            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            ArrayList<JobOffer> jobOffers = new ArrayList<>();
            try {
                jobOffers = jobOfferDAO.getJobOfferListByCompanyId(company.getIdCompany());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            AplicationStatusDAO aplicationStatusDAO = dm.getAplicationStatusDAO();
            ArrayList<AplicationStatus> aplicationStatuses = aplicationStatusDAO.getAllAplicationStatuses();

            for(JobOffer jobOffer : jobOffers) {
                CheckBoxTreeItem<String> jobOfferTreeItem = new CheckBoxTreeItem<>(jobOffer.getnameOfOffer());

                for(AplicationStatus aplicationStatus : aplicationStatuses) {
                    CheckBoxTreeItem<String> aplicationStatusTreeItem = new CheckBoxTreeItem<>(aplicationStatus.getName());
                    jobOfferTreeItem.getChildren().add(aplicationStatusTreeItem);

                    aplicationStatusTreeItem.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {

                        if(aplicationStatusTreeItem.isSelected()) {
                            ArrayList<Integer> aplicationStatusForJobOfferList = new ArrayList<>();
                            if(jobOfferIdAplicationStatusIdMap.get(jobOffer.getIdJobOffer()) != null)
                                aplicationStatusForJobOfferList = jobOfferIdAplicationStatusIdMap.get(jobOffer.getIdJobOffer());
                            aplicationStatusForJobOfferList.add(aplicationStatus.getIdAplicationStatus());
                            jobOfferIdAplicationStatusIdMap.put(jobOffer.getIdJobOffer(), aplicationStatusForJobOfferList);
                        } else {
                            ArrayList<Integer> aplicationStatusForJobOfferList = jobOfferIdAplicationStatusIdMap.get(jobOffer.getIdJobOffer());
                            aplicationStatusForJobOfferList.remove(new Integer(aplicationStatus.getIdAplicationStatus()));
                            jobOfferIdAplicationStatusIdMap.put(jobOffer.getIdJobOffer(), aplicationStatusForJobOfferList);
                        }
                    });
                }
                root.getChildren().add(jobOfferTreeItem);
            }

            AplicationDAO aplicationDAO = dm.getAplicationDAO();
            ArrayList<Aplication> aplications = aplicationDAO.getAplicationListForJobOfferIdsList(jobOffers);
            JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();

            for(Aplication aplication : aplications) {
                Jobseeker jobseeker = jobseekerDAO.getJobseekerById(aplication.getIdJobseeker());
                JobOffer jobOffer = jobOfferDAO.getJobOfferByID(aplication.getIdJobOffer());
                AplicationStatus aplicationStatus = aplicationStatusDAO.getAplicationStatusById(aplication.getIdAplicationStatus());
                Label aplicationLabel = new Label(jobOffer.getnameOfOffer() + ":   " + jobseeker.getName() + " "
                        + jobseeker.getLastName() + "   " + aplicationStatus.getName());

                aplicationLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        if(e.getClickCount() == 2){
                            showOfferAndJobseekerDetails(jobOffer, jobseeker, aplication);
                        }
                    }
                });
                int userID = SignInLogic.loggedUser.getIdUser();
                CompanyDAO cmpDAO = dm.getCompanyDAO();
                int compID = cmpDAO.getCompanyIDByUserID(userID);
                aplicationListView.getItems().add(aplicationLabel);
            }

            CheckTreeView checkTreeView = new CheckTreeView(root);
            checkTreeView.setMaxSize(jobOfferPane.getMaxWidth(), jobOfferPane.getMaxHeight());
            jobOfferPane.getChildren().add(checkTreeView);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setCompany(Company company) {
        AplicationResponseControler.company = company;
    }

    @FXML
    public void eraseBtnAction(){
        erase();
        jobOfferIdAplicationStatusIdMap.clear();
        search();
    }

    @FXML
    public void searchBtnAction(){
        search();
    }

    private void erase() {
        root.setSelected(true);
        root.setSelected(false);
    }

    private void search() {
        aplicationsScrollPane.setContent(aplicationAnchorPane);
        DAOManager dm = DAOManager.getInstance();
        try {
            AplicationDAO aplicationDAO = dm.getAplicationDAO();
            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            AplicationStatusDAO aplicationStatusDAO = dm.getAplicationStatusDAO();
            boolean isHashMapContainsAnyValue = false;
            for (Map.Entry<Integer, ArrayList<Integer>> entry : jobOfferIdAplicationStatusIdMap.entrySet()) {
                if(entry.getValue() != null && !entry.getValue().isEmpty()) {
                    isHashMapContainsAnyValue = true;
                    break;
                }
            }

            if(isHashMapContainsAnyValue) {
                ArrayList<Aplication> aplications = aplicationDAO.getAplicationsListByJobOfferIdAplicationStatuseIdsMap(jobOfferIdAplicationStatusIdMap);
                aplicationListView.getItems().clear();

                JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();

                for(Aplication aplication : aplications) {
                    Jobseeker jobseeker = jobseekerDAO.getJobseekerById(aplication.getIdJobseeker());
                    JobOffer jobOffer = jobOfferDAO.getJobOfferByID(aplication.getIdJobOffer());
                    AplicationStatus aplicationStatus = aplicationStatusDAO.getAplicationStatusById(aplication.getIdAplicationStatus());
                    Label aplicationLabel = new Label(jobOffer.getnameOfOffer() + ":   " + jobseeker.getName() + " "
                            + jobseeker.getLastName() + "   " + aplicationStatus.getName());
                    aplicationLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent e) {
                            if(e.getClickCount() == 2){
                                showOfferAndJobseekerDetails(jobOffer, jobseeker, aplication);
                            }
                        }
                    });

                        aplicationListView.getItems().add(aplicationLabel);

                }
            } else {
                aplicationListView.getItems().clear();
                initialize();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showOfferAndJobseekerDetails(JobOffer jobOffer, Jobseeker jobseeker, Aplication aplication) {
        TabPane tabPane = new TabPane();
        Tab jobOfferDetails = new Tab("Offer");
        Tab jobseekerDetails = new Tab("Jobseeker");
        Label JobseekerName = new Label(jobseeker.getName() + " " + jobseeker.getLastName());
        Label educationLabel = new Label("Education:");
        Label experienceLabel = new Label("Experience:");
        Label knownLanguagesLabel =  new Label("Known languages:");
        VBox educationVBox = new VBox();
        VBox experienceVBox = new VBox();
        VBox knownLanguagesVBox = new VBox();
        VBox jobseekerVBox = new VBox();
        VBox jobOfferVBox = new VBox();
        GridPane gridPane = new GridPane();
        Button backbtn = new Button("Back");
        Button applyBtn = new Button("Apply");
        Label aplicationStatusesLabel = new Label("Application status: ");
        ChoiceBox<String> aplicationsStatusesChoiceBox = new ChoiceBox<>();
        HBox controls = new HBox();
        controls.setSpacing(25.0);
        try {
            DAOManager dm = DAOManager.getInstance();
            EducationDAO educationDAO = dm.getEducationDAO();
            ArrayList<Education> educations = educationDAO.getEducationByJobseekerId(jobseeker.getIdJobseeker());
            for(Education education : educations) {
                Label educationInfo = new Label(education.toString());
                educationVBox.getChildren().add(educationInfo);
            }

            ExperienceDAO experienceDAO = dm.getExperienceDAO();
            ArrayList<Experience> experiences = experienceDAO.getExperiencesByJobseekerId(jobseeker.getIdJobseeker());
            for(Experience experience : experiences) {
                Label experienceInfo = new Label(experience.toString());
                experienceVBox.getChildren().add(experienceInfo);
            }

            KnownLanguageDAO knownLanguageDAO = dm.getKnownLanguageDAO();
            ArrayList<KnownLanguage> knownLanguages = knownLanguageDAO.getKnownLanguageByJobseekerId(jobseeker.getIdJobseeker());
            for(KnownLanguage knownLanguage : knownLanguages) {
                Label knownLanguageLabel = new Label(knownLanguage.toString());
                knownLanguagesVBox.getChildren().add(knownLanguageLabel);
            }

            jobseekerVBox.getChildren().addAll(JobseekerName, experienceLabel, experienceVBox, educationLabel, educationVBox, knownLanguagesLabel, knownLanguagesVBox);

            CompanyDAO companyDAO = dm.getCompanyDAO();
            Company company = companyDAO.getCompanyByID(jobOffer.getIdCompany());
            Label offeraName = new Label(jobOffer.getnameOfOffer());
            Label companyName = new Label("Company: " + company.getName());
            jobOfferVBox.getChildren().add(companyName);

            int numberOfRowSpan = 0;

            if(jobOffer.getMinExpirience() != 0) {
                Label minExpirience = new Label("Min Expirience: " + jobOffer.getMinExpirience());
                jobOfferVBox.getChildren().add(minExpirience);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdPartOfVacancy() != 0) {
                PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
                PartOfVacancy partOfVacancy = partOfVacancyDAO.getPartOfVacancyById(jobOffer.getIdPartOfVacancy());
                Label partOfVacancyLabel = new Label("Part of vacancy: " + partOfVacancy.getName());
                jobOfferVBox.getChildren().add(partOfVacancyLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfEducation() != 0) {
                TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
                TypeOfEducation typeOfEducation = typeOfEducationDAO.getTypeOfEducationById(jobOffer.getIdTypeOfEducation());
                Label typeOfEducationLabel = new Label("Type of Education: " + typeOfEducation.getName());
                jobOfferVBox.getChildren().add(typeOfEducationLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfConcract() != 0) {
                TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
                TypeOfContract typeOfContract = typeOfContractDAO.getTypeOfContractById(jobOffer.getIdTypeOfConcract());
                Label typeOfContractLabel = new Label("Type of contract: " + typeOfContract.getName());
                jobOfferVBox.getChildren().add(typeOfContractLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getIdTypeOfJob() != 0) {
                TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
                TypeOfJob typeOfJob = typeOfJobDAO.getTypeOfJobById(jobOffer.getIdTypeOfJob());
                Label typeOfJobLabel = new Label("Type of job: " + typeOfJob.getName());
                jobOfferVBox.getChildren().add(typeOfJobLabel);
                ++numberOfRowSpan;
            }

            if(jobOffer.getCity() != null) {
                Label city = new Label("City: " + jobOffer.getCity());
                jobOfferVBox.getChildren().add(city);
                ++numberOfRowSpan;
            }

            if(jobOffer.getMinSalary() != 0) {
                Label salary = new Label("Salary: " + jobOffer.getMinSalary());
                jobOfferVBox.getChildren().add(salary);
                ++numberOfRowSpan;
            }

            RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
            if(!requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer()).isEmpty()) {
                HBox requiredLanguages = new HBox();
                VBox requiredLanguagesList = new VBox();
                Label requiredLanguagesLabel = new Label("Required languages: ");

                for(RequiredLanguage requiredLanguage : requiredLanguageDAO.getRequiredLanguagesByJobOfferId(jobOffer.getIdJobOffer())) {
                    Label requiredLanguageLabel = new Label(requiredLanguage.toString());
                    requiredLanguagesList.getChildren().add(requiredLanguageLabel);
                    ++numberOfRowSpan;
                }

                jobOfferVBox.getChildren().add(requiredLanguages);
                requiredLanguages.getChildren().addAll(requiredLanguagesLabel, requiredLanguagesList);
            }

            AplicationStatusDAO aplicationStatusDAO = dm.getAplicationStatusDAO();
            ArrayList<AplicationStatus> aplicationStatuses = aplicationStatusDAO.getAllAplicationStatuses();
            for(AplicationStatus aplicationStatus : aplicationStatuses) {
                aplicationsStatusesChoiceBox.getItems().add(aplicationStatus.getName());
            }
            final String currentAplicationStatus = aplicationStatusDAO.getAplicationStatusById(aplication.getIdAplicationStatus()).getName();
            aplicationsStatusesChoiceBox.setValue(currentAplicationStatus);

            controls.getChildren().addAll(backbtn, aplicationStatusesLabel, aplicationsStatusesChoiceBox, applyBtn);

            backbtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    search();
                }
            });

            applyBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    if(!currentAplicationStatus.equals(aplicationsStatusesChoiceBox.getValue())) {
                        try {
                            AplicationDAO aplicationDAO = dm.getAplicationDAO();
                            int newId = aplicationStatusDAO.getAplicationStatusByName(aplicationsStatusesChoiceBox.getValue()).getIdAplicationStatus();
                            aplicationDAO.updateAplicationStatusId(aplication.getIdAplication(), newId);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });

            gridPane.add(offeraName, 2, 1);
            gridPane.add(jobOfferVBox, 3, 3, 2, numberOfRowSpan);

            jobseekerVBox.getChildren().add(controls);
            jobOfferDetails.setContent(gridPane);
            jobseekerDetails.setContent(jobseekerVBox);
            tabPane.getTabs().addAll(jobseekerDetails, jobOfferDetails);
            tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

            aplicationsScrollPane.setContent(tabPane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}

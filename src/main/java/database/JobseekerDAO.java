package database;

import logic.Experience;
import logic.Jobseeker;

import java.sql.*;

public class JobseekerDAO extends GenericDAO {
    private static String TABLENAME = "Jobseeker";

    public JobseekerDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public void create(Jobseeker jobseeker) throws SQLException {
        String query = "INSERT INTO Jobseeker(DateOfBirth, LastName, Name, User_idUser) VALUES(?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setDate(1, jobseeker.getDateOfBirth());
            preStmt.setString(2, jobseeker.getLastName());
            preStmt.setString(3, jobseeker.getName());
            preStmt.setInt(4, jobseeker.getIdUser());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void insertIntoJobseeker(String name, String lastName, Date date, int idUser) throws SQLException {
        String query = "INSERT INTO Jobseeker(Name, LastName, DateOfBirth, User_idUser) VALUES(?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            preStmt.setString(2, lastName);
            preStmt.setDate(3, date);
            preStmt.setInt(4, idUser);
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public Jobseeker getJobseekerByUserId(int id) throws SQLException {
        String query = "SELECT * FROM Jobseeker WHERE User_idUser=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getJobseekerIdByUserId(int id) throws SQLException {
        String query = "SELECT * FROM Jobseeker WHERE User_idUser=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idJobseeker");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public Jobseeker getJobseekerById(int id) throws SQLException {
        String query = "SELECT * FROM Jobseeker WHERE idJobseeker=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public boolean isJobSeeker(int id) {
       String query = "SELECT * FROM Jobseeker WHERE User_idUser=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);

            ResultSet res = preStmt.executeQuery();
            if(!res.next()) {
                System.out.println("is company");
                return false;
            }
            else {
                System.out.println("is job seeker");
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected Jobseeker mapToObject(ResultSet res) throws SQLException {
        int idJobseeker = res.getInt("idJobseeker");
        Date dateOfBirth = res.getDate("DateOfBirth");
        String lastName = res.getString("LastName");
        String name = res.getString("Name");
        int idUser = res.getInt("User_idUser");

        return new Jobseeker(idJobseeker,dateOfBirth, lastName, name, idUser);
    }
}

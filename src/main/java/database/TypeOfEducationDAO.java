package database;

import logic.Experience;
import logic.TypeOfEducation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TypeOfEducationDAO extends GenericDAO{
    private final static String TABLENAME = "TypeOfEducation";

    public TypeOfEducationDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<TypeOfEducation> getAllTypesOfEducations() throws SQLException {
        String query = "SELECT * FROM TypeOfEducation";
        ArrayList<TypeOfEducation> allTypesOfEducations = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    allTypesOfEducations.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return allTypesOfEducations;
    }

//    public ArrayList<Field> getAllFields() throws SQLException {
//        String query = "SELECT * FROM Field";
//        ArrayList<Field> allFields = new ArrayList<>();
//        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
//            try (ResultSet res = preStmt.executeQuery()) {
//                while (res.next()) {
//                    allFields.add(mapToObject(res));
//                }
//            }
//        } catch (SQLException e) {
//            throw e;
//        }
//        return allFields;
//    }

    public TypeOfEducation getTypeOfEducationById(int id) throws SQLException {
        String query = "SELECT * FROM TypeOfEducation WHERE idTypeOfEducation = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public TypeOfEducation getTypeOfEducationByName(String name) throws SQLException {
        String query = "SELECT * FROM TypeOfEducation WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getTypeOfEducationIdByName(String name) throws SQLException {
        String query = "SELECT * FROM TypeOfEducation WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idTypeOfEducation");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected TypeOfEducation mapToObject(ResultSet res) throws SQLException {
        int idTypeOfEducation = res.getInt("idTypeOfEducation");
        String nameOfEducation = res.getString("Name");

        return new TypeOfEducation(idTypeOfEducation, nameOfEducation);
    }
}

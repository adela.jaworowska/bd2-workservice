package database;

import logic.Experience;
import logic.JobOffer;
import logic.JobOffersFilter;

import java.sql.*;
import java.util.ArrayList;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class JobOfferDAO extends GenericDAO {
    private final static String TABLENAME = "JobOffer";

    public JobOfferDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public void create(JobOffer jobOffer) throws SQLException {
        String query = "INSERT INTO JobOffer(nameOfOffer, DateOfExpiry, MinExpirience, Company_idCompany, " +
                "PartOfVacancy_idPartOfVacancy, TypeOfEducation_idTypeOfEducation, TypeOfContract_idTypeOfContract, " +
                "TypeOfJob_idTypeOfJob, Field_idField, City, MinSalary, DateOfCreation) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query, RETURN_GENERATED_KEYS)) {
            preStmt.setString(1, jobOffer.getnameOfOffer());
            preStmt.setDate(2, jobOffer.getDateOfExpire());
            preStmt.setFloat(3, jobOffer.getMinExpirience());
            preStmt.setInt(4, jobOffer.getIdCompany());

            if(jobOffer.getIdPartOfVacancy() != 0){
                preStmt.setInt(5, jobOffer.getIdPartOfVacancy());
            } else {
                preStmt.setNull(5, java.sql.Types.INTEGER);
            }

            if(jobOffer.getIdTypeOfEducation() != 0){
                preStmt.setInt(6, jobOffer.getIdTypeOfEducation());
            } else {
                preStmt.setNull(6, java.sql.Types.INTEGER);
            }

            if(jobOffer.getIdTypeOfConcract() != 0){
                preStmt.setInt(7, jobOffer.getIdTypeOfConcract());
            } else {
                preStmt.setNull(7, java.sql.Types.INTEGER);
            }

            if(jobOffer.getIdTypeOfJob() != 0){
                preStmt.setInt(8, jobOffer.getIdTypeOfJob());
            } else {
                preStmt.setNull(8, java.sql.Types.INTEGER);
            }

            if(jobOffer.getIdField() != 0){
                preStmt.setInt(9, jobOffer.getIdField());
            } else {
                preStmt.setNull(9, java.sql.Types.INTEGER);
            }

            preStmt.setString(10, jobOffer.getCity());

            if(jobOffer.getMinSalary() != 0){
                preStmt.setFloat(11, jobOffer.getMinSalary());
            } else {
                preStmt.setNull(11, Types.FLOAT);
            }
            java.util.Date utilDate = new java.util.Date();
            Date dateOfCreation = new Date(utilDate.getTime());
            preStmt.setDate(12, dateOfCreation);

            preStmt.executeUpdate();

            ResultSet res = preStmt.getGeneratedKeys();
            res.next();
            jobOffer.setIdJobOffer(res.getInt(1));
            System.out.println(jobOffer.getIdJobOffer());
        } catch (SQLException e) {
            throw e;
        }
    }

    public JobOffer getJobOfferByID(int id) throws SQLException {
        String query = "SELECT * FROM JobOffer WHERE idJobOffer=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public JobOffer getJobOfferByName(String name) throws SQLException {
        String query = "SELECT * FROM JobOffer WHERE nameOfOffer LIKE ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, "%" + name + "%");
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<JobOffer> getListOfJobOffersByName(String name) throws SQLException {
        String query = "SELECT * FROM JobOffer WHERE nameOfOffer LIKE ?";
        ArrayList<JobOffer> jobOffers = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, "%" + name + "%");
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    jobOffers.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return jobOffers;
    }

    public JobOffer getJobOfferByIDTypeOfJob(int id) throws SQLException {
        String query = "SELECT * FROM JobOffer WHERE TypeOfJob_idTypeOfJob=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<JobOffer> getJobOfferListByCompanyId(int id) throws SQLException {
        String query = "SELECT * FROM JobOffer WHERE Company_idCompany=?";
        ArrayList<JobOffer> jobOffers = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    JobOffer jobOffer = mapToObject(res);
                    jobOffers.add(jobOffer);
                }
                return jobOffers;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public void deleteJobOfferById(int id) throws SQLException {
        String query = "DELETE FROM JobOffer WHERE idJobOffer = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            preStmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<JobOffer> getJobOfferListByJobOfferFilter(JobOffersFilter jobOffersFilter) throws SQLException {
        ArrayList<JobOffer> jobOffers = new ArrayList<>();
        String query = getQueryForJobFilter(jobOffersFilter);

        int numberOfFilters = 1;

        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            if(!jobOffersFilter.getNameOfOffer().isEmpty()){
                preStmt.setString(numberOfFilters, "%" + jobOffersFilter.getNameOfOffer() + "%");
                ++numberOfFilters;
            }

            if(jobOffersFilter.getMinExpirience() != 0){
                preStmt.setFloat(numberOfFilters, jobOffersFilter.getMinExpirience());
                ++numberOfFilters;
            }

            if(!jobOffersFilter.getIdPartOfVacancyList().isEmpty()){
                for(int i = 0; i < jobOffersFilter.getIdPartOfVacancyList().size(); ++i){
                    preStmt.setInt(numberOfFilters, jobOffersFilter.getIdPartOfVacancyList().get(i));
                    ++numberOfFilters;
                }
            }

            if(!jobOffersFilter.getIdTypeOfEducationList().isEmpty()){
                for(int i = 0; i < jobOffersFilter.getIdTypeOfEducationList().size(); ++i){
                    preStmt.setInt(numberOfFilters, jobOffersFilter.getIdTypeOfEducationList().get(i));
                    ++numberOfFilters;
                }
            }

            if(!jobOffersFilter.getIdTypeOfConcractList().isEmpty()){
                for(int i = 0; i < jobOffersFilter.getIdTypeOfConcractList().size(); ++i){
                    preStmt.setInt(numberOfFilters, jobOffersFilter.getIdTypeOfConcractList().get(i));
                    ++numberOfFilters;
                }
            }

            if(!jobOffersFilter.getIdTypeOfJobList().isEmpty()){
                for(int i = 0; i < jobOffersFilter.getIdTypeOfJobList().size(); ++i){
                    preStmt.setInt(numberOfFilters, jobOffersFilter.getIdTypeOfJobList().get(i));
                    ++numberOfFilters;
                }
            }

            if(!jobOffersFilter.getIdFieldList().isEmpty()){
                for(int i = 0; i < jobOffersFilter.getIdFieldList().size(); ++i){
                    preStmt.setInt(numberOfFilters, jobOffersFilter.getIdFieldList().get(i));
                    ++numberOfFilters;
                }
            }

            if(!jobOffersFilter.getCity().isEmpty()){
                preStmt.setString(numberOfFilters, jobOffersFilter.getCity());
                ++numberOfFilters;
            }

            if(jobOffersFilter.getMinSalary() != 0){
                preStmt.setFloat(numberOfFilters, jobOffersFilter.getMinSalary());
            }

            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    JobOffer jobOffer = mapToObject(res);
                    DAOManager dm = DAOManager.getInstance();
                    RequiredLanguageDAO requiredLanguageDAO = dm.getRequiredLanguageDAO();
                    if(jobOffersFilter.getIdLanguagesList().isEmpty()){
                        jobOffers.add(jobOffer);
                    }
                     else if(requiredLanguageDAO.isJobOfferRequiresAnyOfLanguages(jobOffer.getIdJobOffer(), jobOffersFilter.getIdLanguagesList())) {
                        jobOffers.add(jobOffer);
                    }
                }
            }
        } catch (SQLException e) {
            throw e;
        }

        return jobOffers;
    }

    private String getQueryForJobFilter(JobOffersFilter jobOffersFilter){
        String query = "SELECT * FROM JobOffer";

        if(!jobOffersFilter.getNameOfOffer().isEmpty()){
            query += " WHERE nameOfOffer LIKE ? AND";
        }

        if(jobOffersFilter.getMinExpirience() != 0){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }
            query += " MinExpirience > ? AND";
        }

        if(!jobOffersFilter.getIdPartOfVacancyList().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }
            query += " PartOfVacancy_idPartOfVacancy IN ( ";

            for(int i = 0; i < jobOffersFilter.getIdPartOfVacancyList().size() - 1; ++i){
                query += "?, ";
            }

            query+="?) AND";
        }

        if(!jobOffersFilter.getIdTypeOfEducationList().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " TypeOfEducation_idTypeOfEducation IN (";

            for(int i = 0; i < jobOffersFilter.getIdTypeOfEducationList().size() - 1; ++i){
                query += "?, ";
            }

            query+="?) AND";
        }

        if(!jobOffersFilter.getIdTypeOfConcractList().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " TypeOfContract_idTypeOfContract IN (";

            for(int i = 0; i < jobOffersFilter.getIdTypeOfConcractList().size() - 1; ++i){
                query += "?, ";
            }

            query+="?) AND";
        }

        if(!jobOffersFilter.getIdTypeOfJobList().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " TypeOfJob_idTypeOfJob IN (";

            for(int i = 0; i < jobOffersFilter.getIdTypeOfJobList().size() - 1; ++i){
                query += "?, ";
            }

            query+="?) AND";
        }

        if(!jobOffersFilter.getIdFieldList().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " Field_idField IN (";

            for(int i = 0; i < jobOffersFilter.getIdFieldList().size() - 1; ++i){
                query += "?, ";
            }

            query+="?) AND";
        }

        if(!jobOffersFilter.getCity().isEmpty()){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " City = ? AND";
        }

        if(jobOffersFilter.getMinSalary() != 0){
            if(!query.contains("WHERE")){
                query += " WHERE";
            }

            query += " MinSalary > ?";
        }

        if(query.substring(query.length() - 3).equals(new String("AND"))){
            return query.substring(0, query.length() - 3);
        }

        return query;
    }

    public void updateJobOffer(JobOffer jobOffer) throws SQLException {
        String query = "UPDATE JobOffer SET nameOfOffer=?, DateOfExpiry=?, MinExpirience=?, PartOfVacancy_idPartOfVacancy=?, " +
                "TypeOfEducation_idTypeOfEducation=?, TypeOfContract_idTypeOfContract=?, TypeOfJob_idTypeOfJob=?, " +
                " Field_idField=?, City=?, MinSalary=? WHERE idJobOffer = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query, RETURN_GENERATED_KEYS)) {
            preStmt.setString(1, jobOffer.getnameOfOffer());
            preStmt.setDate(2, jobOffer.getDateOfExpire());
            preStmt.setFloat(3, jobOffer.getMinExpirience());

            if (jobOffer.getIdPartOfVacancy() != 0) {
                preStmt.setInt(4, jobOffer.getIdPartOfVacancy());
            } else {
                preStmt.setNull(4, java.sql.Types.INTEGER);
            }

            if (jobOffer.getIdTypeOfEducation() != 0) {
                preStmt.setInt(5, jobOffer.getIdTypeOfEducation());
            } else {
                preStmt.setNull(5, java.sql.Types.INTEGER);
            }

            if (jobOffer.getIdTypeOfConcract() != 0) {
                preStmt.setInt(6, jobOffer.getIdTypeOfConcract());
            } else {
                preStmt.setNull(6, java.sql.Types.INTEGER);
            }

            if (jobOffer.getIdTypeOfJob() != 0) {
                preStmt.setInt(7, jobOffer.getIdTypeOfJob());
            } else {
                preStmt.setNull(7, java.sql.Types.INTEGER);
            }

            if (jobOffer.getIdField() != 0) {
                preStmt.setInt(8, jobOffer.getIdField());
            } else {
                preStmt.setNull(8, java.sql.Types.INTEGER);
            }

            preStmt.setString(9, jobOffer.getCity());

            if (jobOffer.getMinSalary() != 0) {
                preStmt.setFloat(10, jobOffer.getMinSalary());
            } else {
                preStmt.setNull(10, Types.FLOAT);
            }

            preStmt.setInt(11, jobOffer.getIdJobOffer());

            preStmt.executeUpdate();
        }
    }

    @Override
    protected JobOffer mapToObject(ResultSet res) throws SQLException {
        int idJobOffer = res.getInt("idJobOffer");
        String nameOfOffer = res.getString("nameOfOffer");
        Date dateOfExpire = res.getDate("DateOfExpiry");
        float minExpirience = res.getFloat("MinExpirience");
        int idCompany = res.getInt("Company_idCompany");
        int idPartOfVacancy = res.getInt("PartOfVacancy_idPartOfVacancy");
        int idTypeOfEducation = res.getInt("TypeOfEducation_idTypeOfEducation");
        int idTypeOfConcract = res.getInt("TypeOfContract_idTypeOfContract");
        int idTypeOfJob = res.getInt("TypeOfJob_idTypeOfJob");
        int idField = res.getInt("Field_idField");
        String city = res.getString("City");
        float minSalary = res.getFloat("MinSalary");

        return new JobOffer(idJobOffer, nameOfOffer, dateOfExpire, minExpirience, idCompany, idPartOfVacancy,
                idTypeOfEducation, idTypeOfConcract, idTypeOfJob, idField, city, minSalary);
    }
}

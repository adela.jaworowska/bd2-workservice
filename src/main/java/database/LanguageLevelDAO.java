package database;

import logic.Experience;
import logic.LanguageLevel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LanguageLevelDAO extends GenericDAO {
    private static String TABLENAME = "LanguageLevel";

    public LanguageLevelDAO(Connection connection) { super(connection, TABLENAME); }

    public LanguageLevel getLanguageLevelById(int id) throws SQLException {
        String query = "SELECT * FROM LanguageLevel WHERE idLanguageLevel=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<LanguageLevel> getAllLanguageLevels() throws SQLException {
        String query = "SELECT * FROM LanguageLevel";
        ArrayList<LanguageLevel> allLanguageLevels = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    allLanguageLevels.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }

        return allLanguageLevels;
    }

    public LanguageLevel getLanguageLevelByName(String name) throws SQLException {
        String query = "SELECT * FROM LanguageLevel WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getLanguageLevelIdByName(String name) throws SQLException {
        String query = "SELECT * FROM LanguageLevel WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idLanguageLevel");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected LanguageLevel mapToObject(ResultSet res) throws SQLException {
        int idLanguageLevel = res.getInt("idLanguageLevel");
        String code = res.getString("Code");
        String name = res.getString("Name");

        return new LanguageLevel(idLanguageLevel, code, name);
    }
}

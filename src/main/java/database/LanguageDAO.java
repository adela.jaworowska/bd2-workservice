package database;

import logic.Experience;
import logic.Language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LanguageDAO extends GenericDAO {
    private static String TABLENAME = "Language";

    public LanguageDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public Language getLanguageById(int id) throws SQLException {
        String query = "SELECT * FROM Language WHERE idLanguage=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<Language> getAllLanguages() throws SQLException {
        String query = "SELECT * FROM Language";
        ArrayList<Language> allLanguages = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    allLanguages.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }

        return allLanguages;
    }

    public Language getLanguageByName(String name) throws SQLException {
        String query = "SELECT * FROM Language WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getLanguageIdByName(String name) throws SQLException {
        String query = "SELECT * FROM Language WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idLanguage");
            }
        } catch (SQLException e) {
            throw e;
        }
    }


    @Override
    protected Language mapToObject(ResultSet res) throws SQLException {
        int idLanguage = res.getInt("idLanguage");
        String name = res.getString("Name");

        return new Language(idLanguage, name);
    }
}

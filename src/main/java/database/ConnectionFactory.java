package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    private final static String driverClassName = "com.mysql.jdbc.Driver";
    private final static String connectionUrl = "jdbc:mysql://80.211.244.164:3306/workservice";
    private final static String dbUser = "workservice";
    private final static String dbPwd = "W1i3eXwDupzDSntG";

    private static ConnectionFactory connectionFactory = null;

    private ConnectionFactory() {
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
        return conn;
    }

    public static ConnectionFactory getInstance() {
        if (connectionFactory == null) {
            connectionFactory = new ConnectionFactory();
        }
        return connectionFactory;
    }
}
package database;

import logic.Experience;
import logic.SignInLogic;
import logic.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO extends GenericDAO {

    private final static String TABLENAME = "User";
    private static SignInLogic signInLogic = new SignInLogic();

    public UserDAO(Connection connection) {
        super(connection, TABLENAME);
    }

    public void create(User user) throws SQLException {
        String query = "INSERT INTO User(Login, Password, Email) VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, user.getLogin());
            preStmt.setString(2, user.getPassword());
            preStmt.setString(3, user.getEmail());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public User getUserByID(int id) throws SQLException {
        String query = "SELECT * FROM User WHERE idUser=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    // while login checks if user exists
    public boolean isUser(String username, String pwd) throws SQLException {
        String queryString = "SELECT * FROM User WHERE Login = ? AND Password = ?";
        try (PreparedStatement ptmt = connection.prepareStatement(queryString)){
            ptmt.setString(1, username);
            ptmt.setString(2, signInLogic.hashFunc(pwd));
            ResultSet resultSet = ptmt.executeQuery();
            if(!resultSet.next())
                return false;
            else
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean loginExists(String username) throws SQLException {
        String queryString = "SELECT * FROM User WHERE Login = ?";
        try (PreparedStatement ptmt = connection.prepareStatement(queryString)){
            ptmt.setString(1, username);
            ResultSet resultSet = ptmt.executeQuery();
            if(!resultSet.next())
                return false;
            else
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void insertIntoUser(String login, String password, String email ) throws SQLException {
        String query = "INSERT INTO User(Login, Password, Email) VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, login);
            preStmt.setString(2, signInLogic.hashFunc(password));
            preStmt.setString(3, email);
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getUserId(String login, String password, String email) throws SQLException {
        String query = "SELECT * FROM User WHERE Login = ? AND Password = ? AND Email = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, login);
            preStmt.setString(2, signInLogic.hashFunc(password));
            preStmt.setString(3, email);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idUser");
            }
        } catch (SQLException e) {
            throw e;
        }
    }
    public int getUserId(String login, String password) throws SQLException {
        String query = "SELECT * FROM User WHERE Login = ? AND Password = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, login);
            preStmt.setString(2, signInLogic.hashFunc(password));
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idUser");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected User mapToObject(ResultSet res) throws SQLException {
        int idUser = res.getInt("idUser");
        String login = res.getString("Login");
        String password = res.getString("Password");
        String email = res.getString("Email");

        return new User(idUser, login, password, email);
    }
}
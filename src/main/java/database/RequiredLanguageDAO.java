package database;

import logic.Experience;
import logic.RequiredLanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RequiredLanguageDAO extends GenericDAO {
    private static String TABLENAME = "RequiredLanguage";

    public RequiredLanguageDAO(Connection connection) { super(connection, TABLENAME);}

    public void create(RequiredLanguage requiredLanguage) throws SQLException {
        String query = "INSERT INTO RequiredLanguage(JobOffer_idJobOffer, LanguageLevel_idLanguageLevel, Language_idLanguage) VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, requiredLanguage.getIdJobOffer());
            preStmt.setInt(2, requiredLanguage.getIdLanguageLevel());
            preStmt.setInt(3, requiredLanguage.getIdLanguage());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<RequiredLanguage> getRequiredLanguagesByJobOfferId(int id) throws SQLException {
        String query = "SELECT * FROM RequiredLanguage WHERE JobOffer_idJobOffer=?";
        ArrayList<RequiredLanguage> allRequiredLanguages = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    allRequiredLanguages.add(mapToObject(res));
                }
                return allRequiredLanguages;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public RequiredLanguage getRquierdLanguagesByJobOfferIdAndLanguageId(int idJobOffer, int idLanguage) throws SQLException {
        String query = "SELECT * FROM RequiredLanguage WHERE JobOffer_idJobOffer=? AND Language_idLanguage=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, idJobOffer);
            preStmt.setInt(2, idLanguage);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()) {
                    return mapToObject(res);
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return null;
    }

    public boolean isJobOfferRequiresAnyOfLanguages(int idJobOffer, ArrayList<Integer> idLanguages) throws SQLException {
        try {
            ArrayList<RequiredLanguage> requiredLanguagesForJobOffer = getRequiredLanguagesByJobOfferId(idJobOffer);

            for(RequiredLanguage requiredLanguageForJobOffer : requiredLanguagesForJobOffer) {
                if(idLanguages.contains(new Integer(requiredLanguageForJobOffer.getIdLanguage()))) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deleteRequiredLanguagesByJobOfferId(int id) throws SQLException {
        String query = "DELETE FROM RequiredLanguage WHERE JobOffer_idJobOffer=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            preStmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void deleteRequiredLanguageById(int id) throws SQLException {
        String query = "DELETE FROM RequiredLanguage WHERE idRequiredLanguage=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            preStmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected RequiredLanguage mapToObject(ResultSet res) throws SQLException {
        int idRequiredLanguage = res.getInt("idRequiredLanguage");
        int idJobOffer = res.getInt("JobOffer_idJobOffer");
        int idLanguageLevel = res.getInt("LanguageLevel_idLanguageLevel");
        int idLanguage = res.getInt("Language_idLanguage");

        return new RequiredLanguage(idRequiredLanguage, idJobOffer, idLanguageLevel, idLanguage);
    }
}

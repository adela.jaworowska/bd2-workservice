package database;

import logic.Experience;
import logic.TypeOfContract;

import java.sql.*;
import java.util.ArrayList;

public class TypeOfContractDAO extends GenericDAO {
    private final static String TABLENAME = "TypeOfContract";

    public TypeOfContractDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<TypeOfContract> getAllTypesOfContracts() throws SQLException {
        String query = "SELECT * FROM TypeOfContract";
        ArrayList<TypeOfContract> allTypesOfContracts = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    allTypesOfContracts.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return allTypesOfContracts;
    }

    public TypeOfContract getTypeOfContractById(int id) throws SQLException {
        String query = "SELECT * FROM TypeOfContract WHERE idTypeOfContract = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public TypeOfContract getTypeOfContractByName(String name) throws SQLException {
        String query = "SELECT * FROM TypeOfContract WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected TypeOfContract mapToObject(ResultSet res) throws SQLException {
        int idTypeOfContract = res.getInt("idTypeOfContract");
        String nameOfContract = res.getString("Name");

        return new TypeOfContract(idTypeOfContract, nameOfContract);
    }
}

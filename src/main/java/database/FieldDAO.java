package database;

import logic.Experience;
import logic.Field;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FieldDAO extends GenericDAO {
    private final static String TABLENAME = "TypeOfContract";

    public FieldDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<Field> getAllFields() throws SQLException {
        String query = "SELECT * FROM Field";
        ArrayList<Field> allFields = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    allFields.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return allFields;
    }

    public Field getFieldById(int id) throws SQLException {
        String query = "SELECT * FROM Field WHERE idField = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public Field getFieldByName(String name) throws SQLException {
        String query = "SELECT * FROM Field WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public int getFieldIdByName(String name) throws SQLException {
        String query = "SELECT * FROM Field WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idField");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected Field mapToObject(ResultSet res) throws SQLException {
        int idField = res.getInt("idField");
        String nameOfField = res.getString("Name");

        return new Field(idField, nameOfField);
    }
}

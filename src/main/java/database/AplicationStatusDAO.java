package database;

import logic.AplicationStatus;
import logic.Experience;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AplicationStatusDAO extends GenericDAO {
    private static String TABLENAME = "AplicationStatus";

    public AplicationStatusDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<AplicationStatus> getAllAplicationStatuses() throws SQLException {
        String query = "SELECT * FROM AplicationStatus";
        ArrayList<AplicationStatus> allAplicationStatus = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    allAplicationStatus.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }

        return allAplicationStatus;
    }

    public AplicationStatus getAplicationStatusById(int id) throws SQLException {
        String query = "SELECT * FROM AplicationStatus WHERE idAplicationStatus=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public AplicationStatus getAplicationStatusByName(String name) throws SQLException {
        String query = "SELECT * FROM AplicationStatus WHERE Name=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected AplicationStatus mapToObject(ResultSet res) throws SQLException {
        int idAplicationStatus = res.getInt("idAplicationStatus");
        String name = res.getString("Name");

        return new AplicationStatus(idAplicationStatus, name);
    }
}

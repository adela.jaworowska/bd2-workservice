package database;

import logic.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Sample {
    public static void main(String[] args) {
        DAOManager dm = DAOManager.getInstance();

        try {
            UserDAO userDAO = dm.getUserDAO();
            User user = userDAO.getUserByID(5);

            JobOfferDAO jobOfferDAO = dm.getJobOfferDAO();
            JobOffer jobOffer = jobOfferDAO.getJobOfferByID(1);

            System.out.println(user);
            System.out.println(userDAO.count());

            System.out.println(jobOffer);

            jobOffer = jobOfferDAO.getJobOfferByIDTypeOfJob(1);
            System.out.println(jobOffer);

            jobOffer = jobOfferDAO.getJobOfferByName("Develope");
            System.out.println(jobOffer);

            ArrayList<JobOffer> listOfJobOffers  = jobOfferDAO.getListOfJobOffersByName("Developer");
            System.out.println(listOfJobOffers.toString());

            JobOffersFilter jobOffersFilter = new JobOffersFilter("Dev", 0, new ArrayList<Integer>(Arrays.asList(1)), new ArrayList<Integer>(Arrays.asList(1)),
                    new ArrayList<Integer>(Arrays.asList(1)), new ArrayList<Integer>(Arrays.asList(1)), new ArrayList<Integer>(Arrays.asList(1)), new ArrayList<Integer>(Arrays.asList(1)), new String(), 0);

            listOfJobOffers = jobOfferDAO.getJobOfferListByJobOfferFilter(jobOffersFilter);
            System.out.println(listOfJobOffers.toString());

            TypeOfContractDAO typeOfContractDAO = dm.getTypeOfContractDAO();
            ArrayList<TypeOfContract> allTypesOfContracts = typeOfContractDAO.getAllTypesOfContracts();
            System.out.println(allTypesOfContracts.toString());

            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            ArrayList<TypeOfEducation> allTypesOfEducations = typeOfEducationDAO.getAllTypesOfEducations();
            System.out.println(allTypesOfEducations.toString());

            TypeOfJobDAO typeOfJobDAO = dm.getTypeOfJobDAO();
            ArrayList<TypeOfJob> allTypesOfJobs = typeOfJobDAO.getAllTypesOfJobs();
            System.out.println(allTypesOfJobs.toString());

            PartOfVacancyDAO partOfVacancyDAO = dm.getPartOfVacancyDAO();
            ArrayList<PartOfVacancy> allTypesOfVacancies = partOfVacancyDAO.getAllPartsOfVacancies();
            System.out.println(allTypesOfVacancies.toString());

            FieldDAO fieldDAO = dm.getFieldDAO();
            ArrayList<Field> allFields = fieldDAO.getAllFields();
            System.out.println(allFields.toString());

            LanguageDAO languageDAO = dm.getLanguageDAO();
            ArrayList<Language> languages = languageDAO.getAllLanguages();
            System.out.println(languages);

            LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            ArrayList<LanguageLevel> languageLevels = languageLevelDAO.getAllLanguageLevels();
            System.out.println(languageLevels);

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package database;

import logic.Company;
import logic.Education;
import logic.KnownLanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class KnownLanguageDAO extends GenericDAO {
    private static String TABLENAME = "KnownLanguage";

    public KnownLanguageDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<KnownLanguage> getKnownLanguageByJobseekerId(int id) throws SQLException {
        String query = "SELECT * FROM KnownLanguage WHERE Jobseeker_idJobseeker=?";
        ArrayList<KnownLanguage> knownLanguages = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    knownLanguages.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return knownLanguages;
    }

    public void create(KnownLanguage knownLanguage) throws SQLException {
        String query = "INSERT INTO KnownLanguage(idJobseeker, idKnownLanguage, idLanguage, idLanguageLevel ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, knownLanguage.getIdJobseeker());
            preStmt.setInt(2, knownLanguage.getIdKnownLanguage());
            preStmt.setInt(3, knownLanguage.getIdLanguage());
            preStmt.setInt(4, knownLanguage.getIdLanguageLevel());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void addNew(int idJobseeker, int idLanguage, int idLanguageLevel ) throws SQLException {
        String query = "INSERT INTO KnownLanguage(LanguageLevel_idLanguageLevel, Jobseeker_idJobseeker, Language_idLanguage ) " +
                "VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, idLanguageLevel);
            preStmt.setInt(2, idJobseeker);
            preStmt.setInt(3, idLanguage);
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected KnownLanguage mapToObject(ResultSet res) throws SQLException {
        int idKnownLanguage = res.getInt("idKnownLanguage");
        int idJobseeker = res.getInt("Jobseeker_idJobseeker");
        int idLanguageLevel = res.getInt("LanguageLevel_idLanguageLevel");
        int idLanguage = res.getInt("Language_idLanguage");

        return new KnownLanguage(idKnownLanguage, idLanguageLevel, idJobseeker, idLanguage);
    }
}

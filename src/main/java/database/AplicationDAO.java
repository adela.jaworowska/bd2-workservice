package database;

import logic.Aplication;
import logic.Experience;
import logic.JobOffer;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class AplicationDAO extends GenericDAO {
    private static String TABLENAME = "Aplication";

    public AplicationDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public void create(Aplication aplication) throws SQLException {
        String query = "INSERT INTO Aplication(Date, JobOffer_idJobOffer, Jobseeker_idJobseeker) VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setDate(1, aplication.getDate());
            preStmt.setInt(2, aplication.getIdJobOffer());
            preStmt.setInt(3, aplication.getIdJobseeker());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public boolean isAplicationExistByJobseekerIdAndJobOfferId(int idJobseeker, int idJobOffer) throws SQLException {
        String query = "SELECT * FROM Aplication WHERE Jobseeker_idJobseeker=? AND JobOffer_idJobOffer=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, idJobseeker);
            preStmt.setInt(2, idJobOffer);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException e) {
            return false;
        }
    }

    public void deleteAplicationByJobOfferId(int id) throws SQLException {
        String query = "DELETE FROM Aplication WHERE JobOffer_idJobOffer=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            preStmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<Aplication> getAplicationListForJobOfferIdsList(ArrayList<JobOffer> jobOffers) throws SQLException {
        ArrayList<Aplication> aplications = new ArrayList<>();
        if(!jobOffers.isEmpty()) {
            String query = "SELECT * FROM Aplication WHERE JobOffer_idJobOffer IN (";
            for (int i = 0; i < jobOffers.size() - 1; ++i) {
                query += "?, ";
            }
            query += "?)";
            try (PreparedStatement preStmt = connection.prepareStatement(query)) {
                for (int i = 0; i < jobOffers.size(); ++i) {
                    preStmt.setInt(i + 1, jobOffers.get(i).getIdJobOffer());
                }
                try (ResultSet res = preStmt.executeQuery()) {
                    while (res.next()) {
                        aplications.add(mapToObject(res));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return aplications;
    }

    public ArrayList<Aplication> getAplicationsListByJobOfferIdAplicationStatuseIdsMap(Map<Integer, ArrayList<Integer>> map) throws SQLException {
        String query = getQueryForJobOfferIdAplicationStatuseIdsMap(map);
        ArrayList<Aplication> aplications = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            int i = 1;
            if(!map.isEmpty()){
                for (Map.Entry<Integer, ArrayList<Integer>> entry : map.entrySet()) {
                    if(entry.getValue() != null && !entry.getValue().isEmpty()) {
                        preStmt.setInt(i, entry.getKey());
                        ++i;
                        for(Integer aplicationStatusId : entry.getValue()) {
                            preStmt.setInt(i, aplicationStatusId);
                            ++i;
                        }
                    }
                }
            }
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    aplications.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aplications;
    }

    private String getQueryForJobOfferIdAplicationStatuseIdsMap(Map<Integer, ArrayList<Integer>> map) {
        String query = "SELECT * FROM Aplication";
        if(!map.isEmpty()){
            for (Map.Entry<Integer, ArrayList<Integer>> entry : map.entrySet()) {
                if(entry.getValue() != null && !entry.getValue().isEmpty()) {
                    if(!query.contains("WHERE")) {
                        query += " WHERE";
                    }
                    query += " (JobOffer_idJobOffer=? AND AplicationStatus_idAplicationStatus IN (";
                    for(int i = 0; i < entry.getValue().size()-1; ++i) {
                        query += " ?,";
                    }
                    query += " ?)) OR";
                }
            }

            if(query.substring(query.length() - 2).equals(new String("OR"))){
                query = query.substring(0, query.length() - 3);
            }
        }

        return query;
    }

    public ArrayList<Aplication> getAplicationsByJobseekerIdAndAplicationStatuseIds(int id, ArrayList<Integer> ids) throws SQLException {
        String query = "SELECT * FROM Aplication WHERE Jobseeker_idJobseeker=?";
        if(!ids.isEmpty()){
            query += " AND AplicationStatus_idAplicationStatus IN (";
            for(int i = 0; i < ids.size()-1; ++i) {
                query +="?, ";
            }
            query += "?)";
        }
        ArrayList<Aplication> aplications = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            for(int i = 0; i < ids.size(); ++i) {
                preStmt.setInt(i+2, ids.get(i));
            }
            try (ResultSet res = preStmt.executeQuery()) {
                while(res.next()) {
                    aplications.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aplications;
    }

    public void updateAplicationStatusId(int aplicationId, int newId) throws SQLException {
        String query = "UPDATE Aplication SET AplicationStatus_idAplicationStatus=? WHERE idAplication=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, newId);
            preStmt.setInt(2, aplicationId);

            preStmt.executeUpdate();
        }
    }

    public void deleteAplicationById(int id) throws SQLException {
        String query = "DELETE FROM Aplication WHERE idAplication = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            preStmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected Aplication mapToObject(ResultSet res) throws SQLException {
        int idAplication = res.getInt("idAplication");
        Date date = res.getDate("Date");
        int idJobOffer = res.getInt("JobOffer_idJobOffer");
        int idJobseeker = res.getInt("Jobseeker_idJobseeker");
        int idAplicationStatus = res.getInt("AplicationStatus_idAplicationStatus");

        return new Aplication(idAplication, date, idJobOffer, idJobseeker, idAplicationStatus);
    }
}

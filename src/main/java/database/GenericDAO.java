package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public abstract class GenericDAO {
    protected final String tableName;
    protected Connection connection;

    public int count() throws SQLException {
        String query = "SELECT COUNT(*) AS count FROM " + tableName;
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("count");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    protected abstract Object mapToObject(ResultSet res) throws SQLException;

    protected Collection mapToObjects(final ResultSet res) throws SQLException {
        Collection result = new ArrayList();
        while (res.next()) {
            result.add(mapToObjects(res));
        }

        return result;
    }

    protected GenericDAO(Connection connection, String tableName) {
        this.tableName = tableName;
        this.connection = connection;
    }

}

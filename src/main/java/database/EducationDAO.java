package database;

import logic.Education;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EducationDAO extends GenericDAO {
    private static String TABLENAME = "Education";

    public EducationDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<Education> getEducationByJobseekerId(int id) throws SQLException {
        String query = "SELECT * FROM Education WHERE Jobseeker_idJobseeker=?";
        ArrayList<Education> educations = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    educations.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return educations;
    }

    public void addNew( int idJobseeker, String school, int startYear, int typeOfEducationId) throws SQLException {
        String query = "INSERT INTO Education(Jobseeker_idJobseeker, School, StartYear, TypeOfEducation_idTypeOfEducation) " +
                "VALUES(?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, idJobseeker );
            preStmt.setString(2, school);
            preStmt.setInt(3, startYear);
            preStmt.setInt(4, typeOfEducationId);

            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected Education mapToObject(ResultSet res) throws SQLException {
        int idEducation = res.getInt("idEducation");
        String school = res.getString("School");
        int startYear = res.getInt("StartYear");
        int endYear = res.getInt("EndYear");
        int idJobseeker = res.getInt("Jobseeker_idJobseeker");
        int idTypeOfEducation = res.getInt("TypeOfEducation_idTypeOfEducation");

        return new Education(idEducation, school, startYear, endYear, idJobseeker, idTypeOfEducation);
    }
}

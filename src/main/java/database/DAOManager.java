package database;

import java.sql.Connection;
import java.sql.SQLException;

public class DAOManager {
    private ConnectionFactory connectionFactory;
    private Connection connection;

    public static DAOManager getInstance() {
        return DAOManagerSingleton.INSTANCE.get();
    }

    public void open() throws SQLException {
        try {
            if (this.connection == null || this.connection.isClosed())
                this.connection = connectionFactory.getConnection();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void close() throws SQLException {
        try {
            if (this.connection != null && !this.connection.isClosed())
                this.connection.close();
        } catch (SQLException e) {
            throw e;
        }
    }

    private void ensureConnection() throws SQLException {
        try {
            if (this.connection == null || this.connection.isClosed()) this.open();
        } catch (SQLException e) {
            throw e;
        }
    }

    public UserDAO getUserDAO() throws SQLException {
        ensureConnection();
        return new UserDAO(this.connection);
    }

    public JobOfferDAO getJobOfferDAO() throws SQLException {
        ensureConnection();
        return new JobOfferDAO(this.connection);
    }

    public TypeOfContractDAO getTypeOfContractDAO() throws SQLException {
        ensureConnection();
        return new TypeOfContractDAO(this.connection);
    }

    public TypeOfEducationDAO getTypeOfEducationDAO() throws SQLException {
        ensureConnection();
        return new TypeOfEducationDAO(this.connection);
    }

    public TypeOfJobDAO getTypeOfJobDAO() throws SQLException {
        ensureConnection();
        return new TypeOfJobDAO(this.connection);
    }

    public PartOfVacancyDAO getPartOfVacancyDAO() throws SQLException {
        ensureConnection();
        return new PartOfVacancyDAO(this.connection);
    }

    public FieldDAO getFieldDAO() throws SQLException {
        ensureConnection();
        return new FieldDAO(this.connection);
    }

    public CompanyDAO getCompanyDAO() throws SQLException {
        ensureConnection();
        return new CompanyDAO(this.connection);
    }

    public AplicationDAO getAplicationDAO() throws SQLException {
        ensureConnection();
        return new AplicationDAO(this.connection);
    }

    public JobseekerDAO getJobseekerDAO() throws SQLException {
        ensureConnection();
        return new JobseekerDAO(this.connection);
    }

    public LanguageDAO getLanguageDAO() throws SQLException {
        ensureConnection();
        return new LanguageDAO(this.connection);
    }

    public LanguageLevelDAO getLanguageLevelDAO() throws SQLException {
        ensureConnection();
        return new LanguageLevelDAO(this.connection);
    }

    public RequiredLanguageDAO getRequiredLanguageDAO() throws SQLException {
        ensureConnection();
        return new RequiredLanguageDAO(this.connection);
    }

    public AplicationStatusDAO getAplicationStatusDAO() throws SQLException {
        ensureConnection();
        return new AplicationStatusDAO(this.connection);
    }

    public ExperienceDAO getExperienceDAO() throws SQLException {
        ensureConnection();
        return new ExperienceDAO(this.connection);
    }

    public EducationDAO getEducationDAO() throws SQLException {
        ensureConnection();
        return new EducationDAO(this.connection);
    }

    public KnownLanguageDAO getKnownLanguageDAO() throws SQLException {
        ensureConnection();
        return new KnownLanguageDAO(this.connection);
    }

    @Override
    protected void finalize() {
//        try {
//            this.close();
//        } finally {
//            super.finalize();
//        }
    }

    private DAOManager() {
        connectionFactory = ConnectionFactory.getInstance();
    }

    private static class DAOManagerSingleton {
        public static final ThreadLocal<DAOManager> INSTANCE;

        static {
            INSTANCE = ThreadLocal.withInitial(DAOManager::new);
        }
    }
}
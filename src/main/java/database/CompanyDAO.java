package database;

import logic.Company;
import logic.Experience;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanyDAO extends GenericDAO {
    private static String TABLENAME = "Company";

    public CompanyDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public void create(Company company) throws SQLException {
        String query = "INSERT INTO JobOffer(idCompnay, Name, NIP, KRS, PhoneNumber, Adviser_idAdviser, User_idUser ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, company.getIdCompany());
            preStmt.setString(2, company.getName());
            preStmt.setString(3, company.getNip());
            preStmt.setString(4, company.getKrs());
            preStmt.setString(5, company.getPhoneNumber());
            preStmt.setInt(6, company.getIdAdviser());
            preStmt.setInt(7, company.getIdUser());
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void insertIntoCompany(String companyName, String NIP, String KRS, String PhoneNumber, int userId) throws SQLException {
        String query = "INSERT INTO Company(Name, NIP, KRS, PhoneNUmber,User_idUser) VALUES(?, ?, ?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, companyName);
            preStmt.setString(2, NIP);
            preStmt.setString(3, KRS);
            preStmt.setString(4, PhoneNumber);
            preStmt.setInt(5, userId);
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public Company getCompanyByID(int id) throws SQLException {
        String query = "SELECT * FROM Company WHERE idCompany=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }
    public int getCompanyIDByUserID(int id) throws SQLException {
        String query = "SELECT idCompany FROM Company WHERE User_idUser=?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return res.getInt("idCompany");
            }
        } catch (SQLException e) {
            throw e;
        }
    }
    @Override
    protected Company mapToObject(ResultSet res) throws SQLException {
        int idCompany = res.getInt("idCompany");
        String name = res.getString("Name");
        String nip = res.getString("NIP");
        String krs = res.getString("KRS");
        String phoneNumber = res.getString("PhoneNumber");
        int idAdviser = res.getInt("Adviser_idAdviser");
        int idUser = res.getInt("User_idUser");

        return new Company(idCompany, name, nip, krs, phoneNumber, idAdviser, idUser);
    }
}

package database;

import logic.Experience;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ExperienceDAO extends GenericDAO {
    private static String TABLENAME = "Experience";

    public ExperienceDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<Experience> getExperiencesByJobseekerId(int id) throws SQLException {
        String query = "SELECT * FROM Experience WHERE Jobseeker_idJobseeker=?";
        ArrayList<Experience> experiences = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    experiences.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return experiences;
    }

    public void addNew(String description, int idField, int idJobseeker ) throws SQLException {
        String query = "INSERT INTO Experience(Description, Field_idField, Jobseeker_idJobseeker) " +
                "VALUES(?, ?, ?)";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, description);
            preStmt.setInt(2, idField);
            preStmt.setInt(3, idJobseeker );
            preStmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected Experience mapToObject(ResultSet res) throws SQLException {
        int idExperience = res.getInt("idExperience");
        String description = res.getString("Description");
        int idField = res.getInt("Field_idField");
        int idJobseeker = res.getInt("Jobseeker_idJobseeker");

        return new Experience(idExperience, description, idField, idJobseeker);
    }
}

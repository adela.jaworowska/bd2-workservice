package database;

import logic.Experience;
import logic.TypeOfJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TypeOfJobDAO extends GenericDAO{
    private final static String TABLENAME = "TypeOfJob";

    public TypeOfJobDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<TypeOfJob> getAllTypesOfJobs() throws SQLException {
        String query = "SELECT * FROM TypeOfJob";
        ArrayList<TypeOfJob> allTypesOfJobs = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    allTypesOfJobs.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return allTypesOfJobs;
    }

    public TypeOfJob getTypeOfJobById(int id) throws SQLException {
        String query = "SELECT * FROM TypeOfJob WHERE idTypeOfJob = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public TypeOfJob getTypeOfJobByName(String name) throws SQLException {
        String query = "SELECT * FROM TypeOfJob WHERE Name = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected TypeOfJob mapToObject(ResultSet res) throws SQLException {
        int idTypeOfJob = res.getInt("idTypeOfJob");
        String nameOfJob = res.getString("Name");

        return new TypeOfJob(idTypeOfJob, nameOfJob);
    }
}

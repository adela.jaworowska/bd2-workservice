package database;

import logic.Experience;
import logic.PartOfVacancy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PartOfVacancyDAO extends GenericDAO {
    private final static String TABLENAME = "PartOfVacancy";

    public PartOfVacancyDAO(Connection connection){
        super(connection, TABLENAME);
    }

    public ArrayList<PartOfVacancy> getAllPartsOfVacancies() throws SQLException {
        String query = "SELECT * FROM PartOfVacancy";
        ArrayList<PartOfVacancy> allTypesOfContracts = new ArrayList<>();
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            try (ResultSet res = preStmt.executeQuery()) {
                while (res.next()) {
                    allTypesOfContracts.add(mapToObject(res));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
        return allTypesOfContracts;
    }

    public PartOfVacancy getPartOfVacancyById(int id) throws SQLException {
        String query = "SELECT * FROM PartOfVacancy WHERE idPartOfVacancy = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setInt(1, id);
            try (ResultSet res = preStmt.executeQuery()) {
                res.next();
                return mapToObject(res);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public PartOfVacancy getPartOfVacancyByName(String name) throws SQLException {
        String query = "SELECT * FROM PartOfVacancy WHERE Part = ?";
        try (PreparedStatement preStmt = connection.prepareStatement(query)) {
            preStmt.setString(1, name);
            try (ResultSet res = preStmt.executeQuery()) {
                if(res.next()){
                    return mapToObject(res);
                }
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    protected PartOfVacancy mapToObject(ResultSet res) throws SQLException {
        int idPartOfVacancy = res.getInt("idPartOfVacancy");
        String partOfVacancy = res.getString("Part");

        return new PartOfVacancy(idPartOfVacancy, partOfVacancy);
    }
}

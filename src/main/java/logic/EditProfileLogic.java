package logic;

import database.*;

import java.sql.SQLException;

public class EditProfileLogic {


    DAOManager dm;

    public EditProfileLogic(){
        dm = DAOManager.getInstance();
    }

    public void addNewKnownLanguage(String language, String level ) throws SQLException {
        LanguageLevelDAO languageLevelDAO= dm.getLanguageLevelDAO();
        LanguageDAO languageDAO = dm.getLanguageDAO();
        KnownLanguageDAO knownLanguageDAO = dm.getKnownLanguageDAO();
        SignInLogic signInLogic = new SignInLogic();
        JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
        System.out.println(signInLogic.getUserId());
        System.out.println(jobseekerDAO.getJobseekerIdByUserId(signInLogic.getUserId()));
        knownLanguageDAO.addNew(jobseekerDAO.getJobseekerIdByUserId(signInLogic.getUserId()), languageDAO.getLanguageIdByName(language), languageLevelDAO.getLanguageLevelIdByName(level));
    }

    public void addNewExperience(String description, String fieldName) throws SQLException {
        ExperienceDAO experienceDAO = dm.getExperienceDAO();
        FieldDAO filedDAO = dm.getFieldDAO();
        JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
        SignInLogic signInLogic = new SignInLogic();

        experienceDAO.addNew(description, filedDAO.getFieldIdByName(fieldName), jobseekerDAO.getJobseekerIdByUserId(signInLogic.getUserId()) );
    }

    public void addNewEducation(String school, int startYear, String typeOfEducation) throws SQLException {
        EducationDAO educationDAO = dm.getEducationDAO();
        JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
        SignInLogic signInLogic = new SignInLogic();
        TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();

        educationDAO.addNew(jobseekerDAO.getJobseekerIdByUserId(signInLogic.getUserId()), school, startYear, typeOfEducationDAO.getTypeOfEducationIdByName(typeOfEducation));
    }
}

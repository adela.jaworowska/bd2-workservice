package logic;

public class Company {
    int idCompany;
    String name;
    String nip;
    String krs;
    String phoneNumber;
    int idAdviser;
    int idUser;

    public Company(int idCompany, String name, String nip, String krs, String phoneNumber, int idAdviser, int idUser) {
        this.idCompany = idCompany;
        this.name = name;
        this.nip = nip;
        this.krs = krs;
        this.phoneNumber = phoneNumber;
        this.idAdviser = idAdviser;
        this.idUser = idUser;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getKrs() {
        return krs;
    }

    public void setKrs(String krs) {
        this.krs = krs;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getIdAdviser() {
        return idAdviser;
    }

    public void setIdAdviser(int idAdviser) {
        this.idAdviser = idAdviser;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }

}

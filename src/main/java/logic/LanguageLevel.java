package logic;

public class LanguageLevel {
    int idLanguageLevel;
    String code;
    String name;

    public LanguageLevel(int idLanguageLevel, String code, String name) {
        this.idLanguageLevel = idLanguageLevel;
        this.code = code;
        this.name = name;
    }

    public int getIdLanguageLevel() {
        return idLanguageLevel;
    }

    public void setIdLanguageLevel(int idLanguageLevel) {
        this.idLanguageLevel = idLanguageLevel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s/%s", name, code);
    }
}

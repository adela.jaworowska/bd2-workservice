package logic;

import database.DAOManager;
import database.LanguageDAO;
import database.LanguageLevelDAO;

import java.sql.SQLException;

public class KnownLanguage {
    int idKnownLanguage;
    int idLanguageLevel;
    int idJobseeker;
    int idLanguage;

    public KnownLanguage(int idKnownLanguage, int idLanguageLevel, int idJobseeker, int idLanguage) {
        this.idKnownLanguage = idKnownLanguage;
        this.idLanguageLevel = idLanguageLevel;
        this.idJobseeker = idJobseeker;
        this.idLanguage = idLanguage;
    }

    public int getIdKnownLanguage() {
        return idKnownLanguage;
    }

    public void setIdKnownLanguage(int idKnownLanguage) {
        this.idKnownLanguage = idKnownLanguage;
    }

    public int getIdLanguageLevel() {
        return idLanguageLevel;
    }

    public void setIdLanguageLevel(int idLanguageLevel) {
        this.idLanguageLevel = idLanguageLevel;
    }

    public int getIdJobseeker() {
        return idJobseeker;
    }

    public void setIdJobseeker(int idJobseeker) {
        this.idJobseeker = idJobseeker;
    }

    public int getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(int idLanguage) {
        this.idLanguage = idLanguage;
    }

    @Override
    public String toString() {
        DAOManager dm = DAOManager.getInstance();
        try{
            LanguageDAO languageDAO = dm.getLanguageDAO();
            Language language = languageDAO.getLanguageById(idLanguage);

            LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            LanguageLevel languageLevel = languageLevelDAO.getLanguageLevelById(idLanguageLevel);

            return String.format("%s: %s", language.getName(), languageLevel.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new String();
    }

}

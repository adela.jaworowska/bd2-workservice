package logic;

public class Language {
    int idLanguage;
    String name;

    public Language(int idLanguage, String name) {
        this.idLanguage = idLanguage;
        this.name = name;
    }

    public int getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(int idLanguage) {
        this.idLanguage = idLanguage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }
}

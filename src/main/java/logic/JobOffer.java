package logic;

import database.CompanyDAO;
import database.DAOManager;

import java.sql.Date;
import java.sql.SQLException;

public class JobOffer {
    private int idJobOffer;
    private String nameOfOffer;
    private Date dateOfExpire;
    private float minExpirience;
    private int idCompany;
    private int idPartOfVacancy;
    private int idTypeOfEducation;
    private int idTypeOfConcract;
    private int idTypeOfJob;
    private int idField;
    private String city;
    private float minSalary;

    public JobOffer(int idJobOffer, String nameOfOffer, Date dateOfExpire, float minExpirience, int idCompany, int idPartOfVacancy,
                    int idTypeOfEducation, int idTypeOfConcract, int idTypeOfJob, int idField, String city, float minSalary){
        this.idJobOffer = idJobOffer;
        this.nameOfOffer = nameOfOffer;
        this.dateOfExpire = dateOfExpire;
        this.minExpirience = minExpirience;
        this.idCompany = idCompany;
        this.idPartOfVacancy = idPartOfVacancy;
        this.idTypeOfEducation = idTypeOfEducation;
        this.idTypeOfConcract = idTypeOfConcract;
        this.idTypeOfJob = idTypeOfJob;
        this.idField = idField;
        this.city = city;
        this.minSalary = minSalary;
    }

    public JobOffer(String nameOfOffer, Date dateOfExpire, float minExpirience, int idCompany, int idPartOfVacancy, int idTypeOfEducation,
                    int idTypeOfConcract, int idTypeOfJob, int idField, String city, float minSalary){
        this.nameOfOffer = nameOfOffer;
        this.dateOfExpire = dateOfExpire;
        this.minExpirience = minExpirience;
        this.idCompany = idCompany;
        this.idPartOfVacancy = idPartOfVacancy;
        this.idTypeOfEducation = idTypeOfEducation;
        this.idTypeOfConcract = idTypeOfConcract;
        this.idTypeOfJob = idTypeOfJob;
        this.idField = idField;
        this.city = city;
        this.minSalary = minSalary;
    }

    public JobOffer() {
        nameOfOffer = new String();
        city = new String();
    }

    public JobOffer(String nameOfOffer, Date dateOfExpire, int idCompany) {
        this.nameOfOffer = nameOfOffer;
        this.dateOfExpire = dateOfExpire;
        this.idCompany = idCompany;
    }
    public int getIdJobOffer() {
        return idJobOffer;
    }

    public void setIdJobOffer(int idJobOffer) {
        this.idJobOffer = idJobOffer;
    }

    public String getnameOfOffer() {
        return nameOfOffer;
    }

    public void setnameOfOffer(String nameOfOffer) {
        this.nameOfOffer = nameOfOffer;
    }

    public Date getDateOfExpire() {
        return dateOfExpire;
    }

    public void setDateOfExpire(Date dateOfExpire) {
        this.dateOfExpire = dateOfExpire;
    }

    public float getMinExpirience() {
        return minExpirience;
    }

    public void setMinExpirience(float minExpirience) {
        this.minExpirience = minExpirience;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public int getIdPartOfVacancy() {
        return idPartOfVacancy;
    }

    public void setIdPartOfVacancy(int idPartOfVacancy) {
        this.idPartOfVacancy = idPartOfVacancy;
    }

    public int getIdTypeOfEducation() {
        return idTypeOfEducation;
    }

    public void setIdTypeOfEducation(int idTypeOfEducation) {
        this.idTypeOfEducation = idTypeOfEducation;
    }

    public int getIdTypeOfConcract() {
        return idTypeOfConcract;
    }

    public void setIdTypeOfConcract(int idTypeOfConcract) {
        this.idTypeOfConcract = idTypeOfConcract;
    }

    public int getIdTypeOfJob() {
        return idTypeOfJob;
    }

    public void setIdTypeOfJob(int idTypeOfJob) {
        this.idTypeOfJob = idTypeOfJob;
    }

    public int getIdField() {
        return idField;
    }

    public void setIdField(int idField) {
        this.idField = idField;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(float minSalary) {
        this.minSalary = minSalary;
    }


    @Override
    public String toString(){
        DAOManager dm = DAOManager.getInstance();
        try {
            CompanyDAO companyDAO = dm.getCompanyDAO();
            String companyName = companyDAO.getCompanyByID(this.idCompany).getName();
            return String.format("%s:  %s", companyName, nameOfOffer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return String.format("%s", nameOfOffer);
    }
}

package logic;


import org.apache.commons.codec.digest.DigestUtils;
import sample.MainViewController;

import java.sql.SQLException;


public class SignInLogic{
    //public static int loggedUserID;
    public static boolean isUserSignedIn;
    public static String loginFirst;
    public static logic.User loggedUser;

    public SignInLogic(){
        isUserSignedIn = false;
    }


    // SHS256 for passwords hashing
    public static String hashFunc(String str){
        return DigestUtils.sha256Hex(str);
    }

    public static void setUserAsLoggedIn(logic.User user, String login)throws SQLException {
        isUserSignedIn = true;
        //loggedUserID = userID;
        loginFirst = login;
        //System.out.println("userid " + loggedUserID);
        loggedUser = user;
        MainViewController.getInstance().changeToSignedUser();
    }

    public static String getUserName(){
        return loggedUser.getLogin();
    }

    public static int getUserId(){
        return loggedUser.getIdUser();
    }

}

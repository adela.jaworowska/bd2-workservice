package logic;

import java.util.ArrayList;

public class JobOffersFilter {
    private String nameOfOffer;
    private float minExpirience;
    private ArrayList<Integer> idPartOfVacancyList;
    private ArrayList<Integer> idTypeOfEducationList;
    private ArrayList<Integer> idTypeOfConcractList;
    private ArrayList<Integer> idTypeOfJobList;
    private ArrayList<Integer> idFieldList;
    private ArrayList<Integer> idLanguagesList;
    private String city;
    private float minSalary;

    public JobOffersFilter(String nameOfOffer, float minExpirience, ArrayList<Integer> idPartOfVacancyList, ArrayList<Integer> idTypeOfEducationList,
                           ArrayList<Integer> idTypeOfConcractList, ArrayList<Integer> idTypeOfJobList, ArrayList<Integer> idFieldList,
                           ArrayList<Integer> idLanguagesList, String city, float minSalary){
        this.nameOfOffer = nameOfOffer;
        this.minExpirience = minExpirience;
        this.idPartOfVacancyList = idPartOfVacancyList;
        this.idTypeOfEducationList = idTypeOfEducationList;
        this.idTypeOfConcractList = idTypeOfConcractList;
        this.idTypeOfJobList = idTypeOfJobList;
        this.idFieldList = idFieldList;
        this.idLanguagesList = idLanguagesList;
        this.city = city;
        this.minSalary = minSalary;
    }

    public JobOffersFilter() {
        this.nameOfOffer = new String();
        this.minExpirience = 0;
        this.idPartOfVacancyList = new ArrayList<>();
        this.idTypeOfEducationList = new ArrayList<>();
        this.idTypeOfConcractList = new ArrayList<>();
        this.idTypeOfJobList = new ArrayList<>();
        this.idFieldList = new ArrayList<>();
        this.idLanguagesList = new ArrayList<>();
        this.city = new String();
        this.minSalary = 0;
    }

    public String getNameOfOffer() {
        return nameOfOffer;
    }

    public void setNameOfOffer(String nameOfOffer) {
        this.nameOfOffer = nameOfOffer;
    }

    public float getMinExpirience() {
        return minExpirience;
    }

    public void setMinExpirience(float minExpirience) {
        this.minExpirience = minExpirience;
    }

    public ArrayList<Integer> getIdPartOfVacancyList() {
        return idPartOfVacancyList;
    }

    public void setIdPartOfVacancyList(ArrayList<Integer> idPartOfVacancyList) {
        this.idPartOfVacancyList = idPartOfVacancyList;
    }

    public ArrayList<Integer> getIdTypeOfEducationList() {
        return idTypeOfEducationList;
    }

    public void setIdTypeOfEducationList(ArrayList<Integer> idTypeOfEducationList) {
        this.idTypeOfEducationList = idTypeOfEducationList;
    }

    public ArrayList<Integer> getIdTypeOfConcractList() {
        return idTypeOfConcractList;
    }

    public void setIdTypeOfConcractList(ArrayList<Integer> idTypeOfConcractList) {
        this.idTypeOfConcractList = idTypeOfConcractList;
    }

    public ArrayList<Integer> getIdTypeOfJobList() {
        return idTypeOfJobList;
    }

    public void setIdTypeOfJobList(ArrayList<Integer> idTypeOfJobList) {
        this.idTypeOfJobList = idTypeOfJobList;
    }

    public ArrayList<Integer> getIdFieldList() {
        return idFieldList;
    }

    public void setIdFieldList(ArrayList<Integer> idFieldList) {
        this.idFieldList = idFieldList;
    }

    public ArrayList<Integer> getIdLanguagesList() {
        return idLanguagesList;
    }

    public void setIdLanguagesList(ArrayList<Integer> idLanguagesList) {
        this.idLanguagesList = idLanguagesList;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(float minSalary) {
        this.minSalary = minSalary;
    }

    public void addIdPartOfVacancy(int idPartOfVacancy) {
        idPartOfVacancyList.add(new Integer(idPartOfVacancy));
    }

    public void removeIdPartOfVacancy(int idPartOfVacancy) {
        idPartOfVacancyList.remove(new Integer(idPartOfVacancy));
    }

    public void addIdTypeOfEducation(int idTypeOfEducation) {
        idTypeOfEducationList.add(new Integer(idTypeOfEducation));
    }

    public void removeIdTypeOfEducation(int idTypeOfEducation) {
        idTypeOfEducationList.remove(new Integer(idTypeOfEducation));
    }

    public void addIdTypeOfConcract(int idTypeOfConcract) {
        idTypeOfConcractList.add(new Integer(idTypeOfConcract));
    }

    public void removeIdTypeOfConcract(int idTypeOfConcract) {
        idTypeOfConcractList.remove(new Integer(idTypeOfConcract));
    }

    public void addIdTypeOfJob(int idTypeOfJob) {
        idTypeOfJobList.add(new Integer(idTypeOfJob));
    }

    public void removeIdTypeOfJob(int idTypeOfJob) {
        idTypeOfJobList.remove(new Integer(idTypeOfJob));
    }

    public void addIdField(int idField) {
        idFieldList.add(new Integer(idField));
    }

    public void removeIdField(int idField) {
        idFieldList.remove(new Integer(idField));
    }

    public void addIdLanguage(int idLanguage) {
        idLanguagesList.add(new Integer(idLanguage));
    }

    public void removeIdLanguage(int idLanguage) {
        idLanguagesList.remove(new Integer(idLanguage));
    }



}

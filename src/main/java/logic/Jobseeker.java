package logic;

import java.sql.Date;

public class Jobseeker {
    private int idJobseeker;
    private Date dateOfBirth;
    private String lastName;
    private String name;
    private int idUser;

    public Jobseeker(int idJobseeker, Date dateOfBirth, String lastName, String name, int idUser) {
        this.idJobseeker = idJobseeker;
        this.dateOfBirth = dateOfBirth;
        this.lastName = lastName;
        this.name = name;
        this.idUser = idUser;
    }

    public Jobseeker(Date dateOfBirth, String lastName, String name, int idUser) {
        this.dateOfBirth = dateOfBirth;
        this.lastName = lastName;
        this.name = name;
        this.idUser = idUser;
    }

    public int getIdJobseeker() {
        return idJobseeker;
    }

    public void setIdJobseeker(int idJobSeeker) {
        this.idJobseeker = idJobSeeker;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return String.format("%d: %s %s", idJobseeker, name, lastName);
    }
}

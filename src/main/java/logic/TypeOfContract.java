package logic;

public class TypeOfContract {
    private int idTypeOfContract;
    private String name;

    public TypeOfContract(int idTypeOfContract, String name) {
        this.idTypeOfContract = idTypeOfContract;
        this.name = name;
    }

    public int getIdTypeOfContract() {
        return idTypeOfContract;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return String.format("%d: %s", idTypeOfContract, name);
    }
}

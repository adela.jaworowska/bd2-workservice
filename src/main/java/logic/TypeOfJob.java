package logic;

public class TypeOfJob {
    private int idTypeOfJob;
    private String name;

    public TypeOfJob(int idTypeOfJob, String name) {
        this.idTypeOfJob = idTypeOfJob;
        this.name = name;
    }

    public int getIdTypeOfJob() {
        return idTypeOfJob;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return String.format("%d: %s", idTypeOfJob, name);
    }
}

package logic;

import database.DAOManager;
import database.FieldDAO;

import java.sql.SQLException;

public class Experience {
    int idExperience;
    String description;
    int idField;
    int idJobseeker;

    public Experience(int idExperience, String description, int idField, int idJobseeker) {
        this.idExperience = idExperience;
        this.description = description;
        this.idField = idField;
        this.idJobseeker = idJobseeker;
    }

    public int getIdExperience() {
        return idExperience;
    }

    public void setIdExperience(int idExperience) {
        this.idExperience = idExperience;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdField() {
        return idField;
    }

    public void setIdField(int idField) {
        this.idField = idField;
    }

    public int getIdJobseeker() {
        return idJobseeker;
    }

    public void setIdJobseeker(int idJobseeker) {
        this.idJobseeker = idJobseeker;
    }

    @Override
    public String toString() {
        DAOManager dm = DAOManager.getInstance();
        try {
            FieldDAO fieldDAO = dm.getFieldDAO();
            Field field = fieldDAO.getFieldById(idField);
            return String.format("%s: \n%s", field.getName(), description);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return description;
    }
}

package logic;

import database.DAOManager;
import database.LanguageDAO;
import database.LanguageLevelDAO;

import java.sql.SQLException;

public class RequiredLanguage {
    int idRequiredLanguage;
    int idJobOffer;
    int idLanguageLevel;
    int idLanguage;

    public RequiredLanguage(int idRequiredLanguage, int idJobOffer, int idLanguageLevel, int idLanguage) {
        this.idRequiredLanguage = idRequiredLanguage;
        this.idJobOffer = idJobOffer;
        this.idLanguageLevel = idLanguageLevel;
        this.idLanguage = idLanguage;
    }

    public RequiredLanguage(int idJobOffer, int idLanguageLevel, int idLanguage) {
        this.idJobOffer = idJobOffer;
        this.idLanguageLevel = idLanguageLevel;
        this.idLanguage = idLanguage;
    }

    public RequiredLanguage(int idLanguageLevel, int idLanguage) {
        this.idLanguageLevel = idLanguageLevel;
        this.idLanguage = idLanguage;
    }

    public int getIdRequiredLanguage() {
        return idRequiredLanguage;
    }

    public void setIdRequiredLanguage(int idRequiredLanguage) {
        this.idRequiredLanguage = idRequiredLanguage;
    }

    public int getIdJobOffer() {
        return idJobOffer;
    }

    public void setIdJobOffer(int idJobOffer) {
        this.idJobOffer = idJobOffer;
    }

    public int getIdLanguageLevel() {
        return idLanguageLevel;
    }

    public void setIdLanguageLevel(int idLanguageLevel) {
        this.idLanguageLevel = idLanguageLevel;
    }

    public int getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(int idLanguage) {
        this.idLanguage = idLanguage;
    }

    @Override
    public String toString() {
        DAOManager dm = DAOManager.getInstance();
        try {
            LanguageLevelDAO languageLevelDAO = dm.getLanguageLevelDAO();
            LanguageDAO languageDAO = dm.getLanguageDAO();

            LanguageLevel languageLevel = languageLevelDAO.getLanguageLevelById(idLanguageLevel);
            Language language = languageDAO.getLanguageById(idLanguage);

            return String.format("%s: %s", language.toString(), languageLevel.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new String("");
    }

    @Override
    public boolean equals(Object o) {
        if(!(o.getClass() == this.getClass())) {
            return false;
        }
        RequiredLanguage requiredLanguage = (RequiredLanguage) o;
        if(!(requiredLanguage.getIdLanguage() == idLanguage)) {
            return false;
        }
        if(!(requiredLanguage.getIdLanguageLevel() == idLanguageLevel)) {
            return false;
        }

        return true;
    }
}

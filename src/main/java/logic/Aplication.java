package logic;

import java.sql.Date;

public class Aplication {
    private int idAplication;
    private Date date;
    private int idJobOffer;
    private int idJobseeker;
    private int idAplicationStatus;

    public Aplication(int idAplication, Date date, int idJobOffer, int idJobseeker, int idAplicationStatus) {
        this.idAplication = idAplication;
        this.date = date;
        this.idJobOffer = idJobOffer;
        this.idJobseeker = idJobseeker;
        this.idAplicationStatus = idAplicationStatus;
    }

    public Aplication(int idAplication, Date date, int idJobOffer, int idJobseeker) {
        this.idAplication = idAplication;
        this.date = date;
        this.idJobOffer = idJobOffer;
        this.idJobseeker = idJobseeker;
    }

    public Aplication(Date date, int idJobOffer, int idJobseeker) {
        this.date = date;
        this.idJobOffer = idJobOffer;
        this.idJobseeker = idJobseeker;
    }

    public int getIdAplication() {
        return idAplication;
    }

    public void setIdAplication(int idAplication) {
        this.idAplication = idAplication;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdJobOffer() {
        return idJobOffer;
    }

    public void setIdJobOffer(int idJobOffer) {
        this.idJobOffer = idJobOffer;
    }

    public int getIdJobseeker() {
        return idJobseeker;
    }

    public void setIdJobSeeker(int idJobseeker) {
        this.idJobseeker = idJobseeker;
    }

    public int getIdAplicationStatus() {
        return idAplicationStatus;
    }

    public void setIdAplicationStatus(int idAplicationStatus) {
        this.idAplicationStatus = idAplicationStatus;
    }

    @Override
    public String toString() {
        return String.format("%d", idAplication);
    }

}

package logic;

public class TypeOfEducation {
    private int idTypeOfEducation;
    private String name;

    public TypeOfEducation(int idTypeOfEducation, String name) {
        this.idTypeOfEducation = idTypeOfEducation;
        this.name = name;
    }

    public int getIdTypeOfEducation() {
        return idTypeOfEducation;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return String.format("%d: %s", idTypeOfEducation, name);
    }
}

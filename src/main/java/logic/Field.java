package logic;

public class Field {
    private int idField;
    private String name;

    public Field(int idField, String name) {
        this.idField = idField;
        this.name = name;
    }

    public int getIdField() {
        return idField;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return String.format("%d: %s", idField, name);
    }
}

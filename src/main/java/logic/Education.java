package logic;

import database.DAOManager;
import database.TypeOfEducationDAO;

import java.sql.SQLException;

public class Education {
    int idEducation;
    String school;
    int startYear;
    int endYear;
    int idJobSeeker;
    int idTypeOfEducation;

    public Education(int idEducation, String school, int startYear, int endYear, int idJobSeeker, int idTypeOfEducation) {
        this.idEducation = idEducation;
        this.school = school;
        this.startYear = startYear;
        this.endYear = endYear;
        this.idJobSeeker = idJobSeeker;
        this.idTypeOfEducation = idTypeOfEducation;
    }

    public int getIdEducation() {
        return idEducation;
    }

    public void setIdEducation(int idEducation) {
        this.idEducation = idEducation;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getIdJobSeeker() {
        return idJobSeeker;
    }

    public void setIdJobSeeker(int idJobSeeker) {
        this.idJobSeeker = idJobSeeker;
    }

    public int getIdTypeOfEducation() {
        return idTypeOfEducation;
    }

    public void setIdTypeOfEducation(int idTypeOfEducation) {
        this.idTypeOfEducation = idTypeOfEducation;
    }

    @Override
    public String toString() {
        DAOManager dm = DAOManager.getInstance();
        try {
            TypeOfEducationDAO typeOfEducationDAO = dm.getTypeOfEducationDAO();
            TypeOfEducation typeOfEducation = typeOfEducationDAO.getTypeOfEducationById(idTypeOfEducation);

            return String.format("%s: %d in %s", typeOfEducation.getName(), startYear, school);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return String.format("%d in %s", startYear, school);
    }
}

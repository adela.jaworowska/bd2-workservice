package logic;

public class AplicationStatus {
    private int idAplicationStatus;
    private String name;

    public AplicationStatus(int idAplicationStatus, String name) {
        this.idAplicationStatus = idAplicationStatus;
        this.name = name;
    }

    public int getIdAplicationStatus() {
        return idAplicationStatus;
    }

    public void setIdAplicationStatus(int idAplicationStatus) {
        this.idAplicationStatus = idAplicationStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }
}

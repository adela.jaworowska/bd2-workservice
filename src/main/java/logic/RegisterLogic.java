package logic;

import database.CompanyDAO;
import database.DAOManager;
import database.JobseekerDAO;
import database.UserDAO;
import java.sql.*;

import static java.lang.Character.isDigit;

public class RegisterLogic {

    DAOManager dm;

    public RegisterLogic(){
        dm = DAOManager.getInstance();
    }

    public void registerPerson(String name, String lastName, String login, String password, String email, Date date) throws SQLException {
        UserDAO userDAO = dm.getUserDAO();
        JobseekerDAO jobseekerDAO = dm.getJobseekerDAO();
        userDAO.insertIntoUser(login, password, email);
        userDAO.getUserId(login, password, email);
        jobseekerDAO.insertIntoJobseeker(name, lastName, date, userDAO.getUserId(login, password, email));
    }

    public void registerCompany(String login, String password, String email, String companyName, String NIP, String KRS, String phoneNumber) throws SQLException {
        UserDAO userDAO = dm.getUserDAO();
        CompanyDAO companyDAO = dm.getCompanyDAO();
        userDAO.insertIntoUser(login, password, email);
        companyDAO.insertIntoCompany(companyName, NIP, KRS, phoneNumber, userDAO.getUserId(login, password, email));
    }

    public Date createDate(int day, int month, int year){
        Date date = new Date(year,month, day);
        System.out.println("Date: " + date);
        return date;
    }

    // to implement - optionally
    public boolean isPersonDataCorrect(String name, String lastName, String login, String password, String email, Date date){

        return true;
    }

    public boolean isCompanyDataCorrect(String login, String password, String email, String companyName, String NIP, String KRS, String phoneNumber){
        if(!isNIPcorrect(NIP) || !isKRScorrect(KRS) || !isPhoneNumberCorrect(phoneNumber))
            return false;
        return true;
    }

    public boolean isNIPcorrect(String NIP){
        if(NIP.length()!= 13)
            return false;
        if(NIP.charAt(3) !='-' || NIP.charAt(6) != '-' || NIP.charAt(9) != '-')
            return false;
        if(!isDigit(NIP.charAt(0)) || !isDigit(NIP.charAt(1)) ||!isDigit(NIP.charAt(2)) ||!isDigit(NIP.charAt(4)) ||!isDigit(NIP.charAt(5)) ||!isDigit(NIP.charAt(7)) ||!isDigit(NIP.charAt(8)) ||!isDigit(NIP.charAt(10)) ||!isDigit(NIP.charAt(11)) ||!isDigit(NIP.charAt(12)))
            return false;

        return true;
    }

    public boolean isKRScorrect(String KRS){
        if(KRS.length()!= 10)
            return false;
        if(!isDigit(KRS.charAt(0)) || !isDigit(KRS.charAt(1)) || !isDigit(KRS.charAt(2)) || !isDigit(KRS.charAt(3)) || !isDigit(KRS.charAt(4)) || !isDigit(KRS.charAt(5)) || !isDigit(KRS.charAt(6)) || !isDigit(KRS.charAt(7)) || !isDigit(KRS.charAt(8)) || !isDigit(KRS.charAt(9)))
            return false;

        return true;
    }

    public boolean isPhoneNumberCorrect(String PhoneNum){
        if(PhoneNum.length()!= 12)
            return false;
        if(PhoneNum.charAt(3) !='-' || PhoneNum.charAt(7) != '-' )
            return false;
        if(!isDigit(PhoneNum.charAt(0)) || !isDigit(PhoneNum.charAt(1)) || !isDigit(PhoneNum.charAt(2)) || !isDigit(PhoneNum.charAt(4)) || !isDigit(PhoneNum.charAt(5)) || !isDigit(PhoneNum.charAt(6)) || !isDigit(PhoneNum.charAt(8)) || !isDigit(PhoneNum.charAt(9)) || !isDigit(PhoneNum.charAt(10)) || !isDigit(PhoneNum.charAt(11)))
            return false;

        return true;
    }


}



package logic;

public class PartOfVacancy {
    private int idPartOfVacancy;
    private String part;

    public PartOfVacancy(int idPartOfVacancy, String part) {
        this.idPartOfVacancy = idPartOfVacancy;
        this.part = part;
    }

    public int getIdPartOfVacancy() {
        return idPartOfVacancy;
    }

    public String getName() {
        return part;
    }

    @Override
    public String toString(){
        return String.format("%d: %s", idPartOfVacancy, part);
    }
}
